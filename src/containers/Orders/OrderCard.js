import React from 'react';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import SwapVertIcon from '@material-ui/icons/SwapVert';
import IconButton from '@material-ui/core/IconButton';
import moment from 'moment';
import OrderDialog from './OrderDialog';
import Add from '@material-ui/icons/Add';
import axios from '../../utils/axios';
import * as actionTypes from '../../Store/actions/actions';
import Divier from '@material-ui/core/Divider';
import { useSelector, useDispatch } from 'react-redux';
import Login from '../../components/Login/Login';
import Signup from '../../components/Signup/Signup'
import Modal from '../../components/Modal/Modal';

const useStyles = makeStyles((theme) => ({
    root: {
        // outline: '1px solid green',
        margin: 'auto',
        width: '50%',
        // padding: '10px',
        borderRadius: '20px',
        margin: 'auto',
        width: '50%',
        padding: '20px 20px',
        borderRadius: '10px',
    },
    avatars: {
        display: 'flex',
        flexDirection: 'row',
        '& > *': {
            margin: theme.spacing(1),
        },
    },
    icon: {
        backgroundColor: '#ffc61a',
        color: 'black',
        padding: 10,
        borderRadius: 30,
        margin: 10,
        marginLeft: 20,
        fontSize: '3em'
    }

}));

const HtmlTooltip = withStyles((theme) => ({
    tooltip: {
        backgroundColor: '#f5f5f9',
        color: 'rgba(0, 0, 0, 0.87)',
        maxWidth: 220,
        fontSize: theme.typography.pxToRem(12),
        border: '1px solid #dadde9',
    },
}))(Tooltip);

const OrderCard = ({ service, users, fetchServices }) => {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [openRegistration, setOpenRegistration] = React.useState(false);
    const dispatch = useDispatch();
    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };
    const user = useSelector(state => state.user)
    const [elevation, setElevation] = React.useState(3);
    const [passingers, setPassingers] = React.useState([]);
    const fetchPassingersForChoosenService = async (id) => {
        const result = await axios.get(`/usersToServices/usersAssignedToService/${id}?requestId=${1}`).then(res => res.data.result);

        setPassingers(result[0]?.service?.users);
    };
    const assignPassigner = async (serviceId, userId, startAdress, endAdress) => {
        await axios.post('/usersToServices', { service_id: serviceId, user_id: userId, start_adress: startAdress, end_adress: endAdress, request_status_id: 3 })
            .then(() => {
                handleClose();
                dispatch({
                    type: actionTypes.SET_MSG_TYPE,
                    msgType: 'success',
                })
                dispatch({
                    type: actionTypes.SET_ALERT_MESSAGE,
                    message: ('Uspesno ste zakazali voznju. Status vase voznje mozete pratati na stranici moje voznje.'),
                });
                setTimeout(() => {
                    dispatch({
                        type: actionTypes.SET_ALERT_MESSAGE,
                        message: '',
                    });
                }, 5000);
            })
            .catch((err) => {
                handleClose();
                dispatch({
                    type: actionTypes.SET_MSG_TYPE,
                    msgType: 'error',
                })
                dispatch({
                    type: actionTypes.SET_ALERT_MESSAGE,
                    message: ('Zahtev za ovu voznju je vec poslat !'),
                });
                setTimeout(() => {
                    dispatch({
                        type: actionTypes.SET_ALERT_MESSAGE,
                        message: '',
                    });
                }, 5000);
            })
    }
    React.useEffect(() => {
        fetchPassingersForChoosenService(service.id)
    }, [service])
    console.log("PUTNICII", passingers)

    return (

        <Paper className={classes.root} onMouseEnter={() => setElevation(10)}
            onMouseLeave={() => setElevation(3)} elevation={elevation}>
            <Grid container spacing={3}>
                <Grid item xs={12} md={4} >
                    <Grid container spacing={1}>
                        <Grid style={{ textAlign: 'left', fontSize: '18px' }} item xs={12} md={7} >Polazak: {moment(service.time).format("HH:mm")}</Grid>
                        <Grid item xs={12} md={5} style={{ fontSize: '18px' }}>{service?.start_des?.city}</Grid>
                    </Grid>
                    <Grid container spacing={1}>
                        <Grid item xs={12}  ><div style={{ height: '15px' }}></div></Grid>

                    </Grid>
                    <Grid container spacing={1}>
                        <Grid style={{ textAlign: 'left', fontSize: '18px' }} item xs={12} md={7} >Dolazak: {moment(service.time_end).format("HH:mm")}</Grid>
                        <Grid item xs={12} md={5} style={{ fontSize: '18px' }}>{service?.end_des?.city}</Grid>
                    </Grid>
                </Grid>
                <Grid item xs={12} md={4} >
                    <Grid container spacing={1}>
                    </Grid>
                </Grid>
                <Grid item xs={12} md={4} >
                    <Grid container spacing={1}>
                        <Grid item xs={12} md={12} style={{ fontSize: '22px' }}><span>Cena: {service.price} din.</span>
                            <Tooltip title="Zakazi voznju"><IconButton color="primary" size="small" aria-label="add to shopping cart">
                                <Add className={classes.icon} onClick={() => {

                                    handleClickOpen();


                                    //assignPassigner(service.id, user.user_id); fetchServices()
                                }} />
                            </IconButton></Tooltip>
                        </Grid>

                    </Grid>
                </Grid>
            </Grid>
            <div style={{ width: '100%', height: "20px" }}></div>
            <Divier></Divier>

            <Grid container spacing={3}>

                <Grid item xs={12} md={12} >
                    <Grid container spacing={1}>
                        <Grid item xs={6} md={6} style={{ display: 'flex', justifyContent: 'flex-start' }} > <em>Putnici:</em></Grid>
                    </Grid>
                    <Grid style={{ display: 'flex' }} item xs={12} md={12} >
                        {passingers?.map((el) => {
                            if (el.map_users_to_services.request_status_id === 1)
                                return (
                                    <div className={classes.avatars}>
                                        <HtmlTooltip
                                            title={
                                                <React.Fragment>
                                                    <Typography color="inherit">{el.full_name}</Typography>
                                                    <em>{el.description}</em>
                                                </React.Fragment>
                                            }
                                        >
                                            <Avatar alt="Remy Sharp" src={el?.picture_path} />
                                        </HtmlTooltip>




                                    </div>
                                )
                        })}
                    </Grid>
                </Grid>
            </Grid>
            {open && user ? (<OrderDialog open={open}
                service={service}
                user={user}
                fetchServices={fetchServices}
                assignPassigner={assignPassigner}
                handleClose={handleClose} />) : open && !user ? <Modal isOpen={open} handleClose={handleClose}><Login handleCloseLoginModal={handleClose}
                    handleOpenRegistrationModal={()=> setOpenRegistration(true)} /> </Modal> : null}

            {openRegistration ? (<Modal isOpen={openRegistration}
                handleClose={() => setOpenRegistration(false)}>
                <Signup handleCloseRegistrationModal={() => setOpenRegistration(false)}
                    handleOpenLoginModal={() => setOpen(true)} />
            </Modal>) : null}
        </Paper>



    )
}

export default OrderCard;