import React, { useEffect } from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from '@date-io/date-fns';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import moment from 'moment';
import axios from '../../utils/axios';
import HighlightOff from '@material-ui/icons/HighlightOff';
import Autocomplete from '@material-ui/lab/Autocomplete';

const useStyles = makeStyles((theme) => ({
    root: {

        margin: 'auto',
        width: '50%',
        padding: '0px 0px 20px 60px',
        borderRadius: '10px',
        backgroundColor: '#ffc61a',
    },


}));


const Filters = ({ setStartLocation, startLocation, locations, endLocation, setEndLocation, fetchServices, date, setDate }) => {
    const classes = useStyles();
    const resetFilters = () => {
        setStartLocation(-1);
        setEndLocation(-1);
        setDate(moment(new Date()).format('YYYY-MM-DD'))
    }
    const [selectedDate, handleDateChange] = React.useState(new Date());
    return (
        <div className={classes.root}>
            <Grid container spacing={2}>

                <Grid item xs={12} md={3}>

                    <Autocomplete
                        options={locations}
                        getOptionLabel={(option) => option.city}
                        id="debug"
                        onChange={(event, value) => setStartLocation(value)}
                        value={startLocation}
                        renderInput={(params) => <TextField {...params} label="Pocetna lokacija" margin="normal" />}
                    />

                </Grid>
                <Grid item xs={12} md={3}>

                    <Autocomplete
                        options={locations}
                        getOptionLabel={(option) => option.city}
                        id="debug"
                        onChange={(event, value) => setEndLocation(value)}
                        value={endLocation}
                        renderInput={(params) => <TextField {...params} label="Krajnja lokacija" margin="normal" />}
                    />

                </Grid>
                <Grid item xs={12} md={3}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                            clearable
                            style={{ marginTop: '32px', marginBottom: '8px' }}
                            value={date}
                            placeholder="10/10/2018"
                            onChange={date => setDate(date)}
                            //minDate={new Date()}
                            format="MM/dd/yyyy"
                        />
                    </MuiPickersUtilsProvider>

                </Grid>
                <Grid direction="row" item xs={3} md={3}>
                    <Tooltip style={{ marginTop: '25px' }} title="Obriši filtere">
                        <IconButton onClick={() => resetFilters()}>
                            <HighlightOff />
                        </IconButton>
                    </Tooltip>

                    {/* <Button
                        variant="contained"
                        color="primary"
                        size="small"
                        style={{marginTop:'16px'}}
                        className={classes.button}
                        startIcon={<SaveIcon />}
                    >
                        Primeni filtere
      </Button> */}

                </Grid>


            </Grid>
        </div>
    )


}

export default Filters;