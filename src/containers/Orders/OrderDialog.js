import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';
import MenuItem from '@material-ui/core/MenuItem';

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));
export default function OrderDialog({ open, handleClose, service, assignPassigner, fetchServices, user }) {
    const classes = useStyles();
    const [startAdress, setStartAdress] = useState(null);
    const [endAdress, setEndAdress] = useState(null);



    return (
        <div>

            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Zakazivanje voznje</DialogTitle>
                <DialogContent>
                    <Grid container spacing={2}>


                        <Grid item xs={12} md={6}>
                            <FormControl fullWidth className={classes.formControl}>
                                <InputLabel id="demo-simple-select-label">Pocetna lokacija</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    //value={age}
                                    //onChange={handleChange}
                                    defaultValue={10}
                                >
                                    <MenuItem value={10}>{service?.start_des?.city}</MenuItem>

                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item xs={12} md={6}>
                            <TextField onChange={(e) => setStartAdress(e.target.value)} value={startAdress} id="filled-basic" label={`Pocetna adresa`} helperText={service?.start_des?.city} style={{ marginTop: '8px' }} />
                        </Grid>

                    </Grid>
                    <Grid container spacing={2}>


                        <Grid item xs={12} md={6}>
                            <FormControl fullWidth disa className={classes.formControl}>
                                <InputLabel id="demo-simple-select-label">Krajnja lokacija</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"

                                    //value={age}
                                    //onChange={handleChange}
                                    defaultValue={10}
                                >
                                    <MenuItem value={10}>{service?.end_des?.city}</MenuItem>

                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item xs={12} md={6}>
                            <TextField onChange={(e) => setEndAdress(e.target.value)} value={endAdress} id="filled-basic" label={`Krajnja adresa`} helperText={service?.end_des?.city} style={{ marginTop: '8px' }} />
                        </Grid>

                    </Grid>

                </DialogContent>
                <DialogActions style={{ margin: 'auto' }}>
                    <Button disabled={startAdress === null || endAdress === null} onClick={() => {
                        assignPassigner(service.id, user.user_id, startAdress, endAdress); fetchServices();
                    }} color="primary">
                        Sacuvaj
                    </Button>
                    <Button onClick={handleClose} color="primary">
                        Otkazi
                    </Button>
                </DialogActions>
            </Dialog>
        </div >
    );
}
