import React from 'react';
import OrderCard from './OrderCard';
import Filters from './Filters';
import axios from '../../utils/axios';
import moment from 'moment';

const isToday = (someDate) => {
  const today = new Date()
  return someDate.getDate() == today.getDate() &&
    someDate.getMonth() == today.getMonth() &&
    someDate.getFullYear() == today.getFullYear()
}



export default function Orders() {
  const [services, setServices] = React.useState([]);
  const [startLocation, setStartLocation] = React.useState(-1);
  const [date, setDate] = React.useState(new Date())
  const [endLocation, setEndLocation] = React.useState(-1);
  const [locations, setLocations] = React.useState([]);
  const fetchLocations = async () => { const result = await axios.get('/locations').then(res => res.data.result); setLocations(result) };
  const fetchServices = async () => {
    const result = await axios.get(`services/?start_location=${startLocation?.id}&end_location=${endLocation?.id}&date=${moment(date).format('YYYY-MM-DD')}`).then(res => res.data.result);
    setServices(result);
  };
  React.useEffect(() => {
    //fetchPassingersForChoosenService();.
    fetchServices()
    fetchLocations();
  }, [startLocation, date, endLocation])
  console.log("SERVISI", services)
  return (
    <React.Fragment >
      <Filters date={date}
        setDate={setDate}
        locations={locations}
        fetchServices={fetchServices} setStartLocation={setStartLocation} endLocation={endLocation} setEndLocation={setEndLocation} startLocation={startLocation} />
      <div style={{ width: '100%', height: "20px" }}></div>
      {services.map((el) => {
        return (<React.Fragment><OrderCard fetchServices={fetchServices} service={el} /><br></br></React.Fragment>)
      })}

    </React.Fragment>
  );
}