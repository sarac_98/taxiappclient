import React from 'react';
import { Route, Switch, useLocation } from 'react-router-dom';
import { useSelector } from "react-redux";
import { useHistory } from 'react-router-dom';
//Material UI components
import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles } from '@material-ui/core/styles';
//My Components
import NavigationBar from '../../components/NavigationBar/NavigationBar';
import SideNavigationBar from '../../components/SideNavigationBar/SideNavigationBar';
import Footer from '../../components/Footer/Footer'
import Spinner from '../../components/Spinner/Spinner';
import Home from '../Home/Home';
import User from '../Admin/User/User';
import Ads from '../Admin/Ads/Ads';
import Vehicle from '../Admin/Vehicle/Vehicle';
import Location from '../Admin/Location/Location';
import Service from '../Admin/Service/Service';
import Orders from '../Orders/Orders';
import ForgotPassword from '../../components/Login/ForgottenPassword/ForgottenPassword';
import ResetPassword from '../../components/Login/resetPassword/resetPassword';
import Profile from '../UserMenu/Profile/Profile';
import Signup from '../../components/Signup/Signup';
import MyServices from '../UserMenu/MyServices/MyServices';
import store from '../../Store/store';
import * as  actionTypes from '../../Store/actions/actions';
import axios from '../../utils/axios';

// const Home = lazy(()=> import('../Home/Home'));
// const User = lazy(()=> import('../Admin/User/User'));
// const Orders = lazy(()=> import('../Orders/Orders'));
// const Ads = lazy(()=> import('../Admin/Ads/Ads'));
// const Service = lazy(()=>import('../Admin/Service/Service'));
// const Vehicle = lazy(()=>import('../Admin/Vehicle/Vehicle'));
// const Location = lazy(()=> import('../Admin/Location/Location'));
// const ForgotPassword = lazy(()=>import('../../components/Login/ForgottenPassword/ForgottenPassword'));
// const ResetPassword = lazy(()=>import('../../components/Login/resetPassword/resetPassword'));
// const Signup = lazy(()=> import('../../components/Signup/Signup'));
// const Profile = lazy(()=> import('../Profile/Profile'))



const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {

      flexShrink: 0,
    },
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
  },
  content: {
    flexGrow: 1,
    width: '90%',
    zIndex: '0',
    padding: theme.spacing(3),
    overflowX: 'hidden'
  },
}));
export default Main; function Main(props) {
  const classes = useStyles();
  const history = useHistory();
  const location = useLocation();
  const [isAdminOpen, setIsAdminOpen] = React.useState(false);
  const user = useSelector(state => state.user);
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const [isOpenLoginModal, toggle] = React.useState(false);
  const [isOpenRegistrationModal, setIsOpenRegistrationModal] = React.useState(false);
  const [mobileMenu, setMobileMenu] = React.useState(null);
  const [userMenu, setUserMenu] = React.useState(null);



  const handleClickUserMenu = (event) => {
    setUserMenu(event.currentTarget);
  };
  const handleCloseUserMenu = () => {
    setUserMenu(null);
  };
  const handleOpenAdminPanel = () => {
    setIsAdminOpen(!isAdminOpen);
  }
  const logOut = () => {
    store.dispatch({
      type: actionTypes.SET_USER,
      user: null,
    })
    delete localStorage.jwtToken;
    return history.push('/');
  }
  const handleClick = (event) => {
    setMobileMenu(event.currentTarget);
  };
  const handleClose = () => {
    setMobileMenu(null);
  };

  function handleOpenRegistrationModal() {
    setIsOpenRegistrationModal(true);
  }
  function handleCloseRegistrationModal() {
    setIsOpenRegistrationModal(false);
  }

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  const handleOpenLoginModal = () => {
    console.log("cao")
    toggle(true);
  }
  const handleCloseLoginModal = () => {
    toggle(false);
  }

  const checkIfHome = () => {
    return location.pathname === '/' ? { padding: 0 } : {}
  }

  return (
    <div className={classes.root}>
      <CssBaseline />

      <NavigationBar
        userMenu={userMenu}
        setUserMenu={setUserMenu}

        user={user}
        logOut={logOut}
        handleOpenLoginModal={handleOpenLoginModal}
        handleCloseLoginModal={handleCloseLoginModal}
        handleClickUserMenu={handleClickUserMenu}
        handleCloseUserMenu={handleCloseUserMenu}
        isOpenRegistrationModal={isOpenRegistrationModal}
        handleOpenRegistrationModal={handleOpenRegistrationModal}
        handleCloseRegistrationModal={handleCloseRegistrationModal}
        openLogin={isOpenLoginModal}
        openRegistration={isOpenRegistrationModal}
        handleDrawerToggle={handleDrawerToggle}
        mobileMenu={mobileMenu}
        handleClick={handleClick}
        handleClose={handleClose} />

      {user && user.user_type == 1 ? <SideNavigationBar
        mobileOpen={mobileOpen}
        handleOpenAdminPanel={handleOpenAdminPanel}
        isAdminOpen={isAdminOpen}
        setMobileOpen={setMobileOpen}
        handleDrawerToggle={handleDrawerToggle} /> : null}


      <main className={classes.content} style={checkIfHome()}>

        <div className={classes.toolbar} />

        <Switch>
          <Route path="/" component={Home} exact />
          <Route path="/signup" component={Signup} exact />
          <Route path="/orders" component={Orders} exact />
          <Route path="/admin/users" component={User} exact />
          <Route path="/profile" component={Profile} exact />
          <Route path="/myservices" component={MyServices} exact />
          <Route path="/admin/ads" component={Ads} exact />
          <Route path="/admin/vehicles" component={Vehicle} exact />
          <Route path="/admin/locations" component={Location} exact />
          <Route path="/admin/services" component={Service} exact />
          <Route path="/forgotPassword" component={ForgotPassword} />
          <Route path="/resetPassword/:token" component={ResetPassword} />


        </Switch>
        {/* <Footer/> */}
      </main>


    </div>
  );
}
