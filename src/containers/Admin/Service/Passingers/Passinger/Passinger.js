import React from 'react';
//Material ui components
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
//Icons
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import CancelIcon from '@material-ui/icons/Cancel';
//Styles
import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
}));
//COMPONENTS PROPS updateRequestStatus(),request
export default function Passinger(props) {
  const classes = useStyles();
  console.log("putnik", props.passingers)
  return (
    <div className={classes.root}>
      <Accordion>
        <AccordionSummary
          style={{ display: 'flex', flexDirection: 'row', alignContent:'flex-end'}}
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <h4 style={{ margin: '0px', padding: '0px' }}>{props.passingers?.full_name}  </h4>
          {props.passingers.map_users_to_services.request_status_id === 3 ? (
            <div style={{float:'right'}}><CheckCircleOutlineIcon
              onClick={(event) => {
                event.stopPropagation();
                props.updateRequestStatus(props.service.id, props.passingers.user_id, 1)
              }} /><CancelIcon onClick={(event) => event.stopPropagation()} />
            </div>
          ) : null}
        </AccordionSummary>
        <AccordionDetails>
          <Typography tyle={{ display: 'flex', flexDirection: 'row', alignContent: 'space-around' }} >
            Početna destinacija: {props.passingers.map_users_to_services.start_adress}({props.service.start_des.city})<br />
            Krajnja destinacija: {props.passingers.map_users_to_services.end_adress}({props.service.end_des.city})<br />
            Kontakt telefon: {props.passingers.phone_number} <br />
            Prtljag: 4 kofera <br />
            Email: {props.passingers.username} <br />
          </Typography>
        </AccordionDetails>
      </Accordion>
    </div>
  );
}