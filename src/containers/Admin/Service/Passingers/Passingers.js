import React, { useState } from 'react';
//My components
import Passinger from './Passinger/Passinger';
//Material up components
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Box from '@material-ui/core/Box';
// Icons
import PhoneIcon from '@material-ui/icons/Phone';
import FavoriteIcon from '@material-ui/icons/Favorite';
//Styles
const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
}));
// Material ui functions
function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-force-tabpanel-${index}`}
            aria-labelledby={`scrollable-force-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}
TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};
function a11yProps(index) {
    return {
        id: `scrollable-force-tab-${index}`,
        'aria-controls': `scrollable-force-tabpanel-${index}`,
    };
}
//COMPONENTS PROPS updateRequestStatus(),request,passingers,requestedUsers
export default function Passingers(props) {
    const classes = useStyles();
    //Value for current selected tab
    const [value, setValue] = useState(0);
    //Functions for changing tab
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    console.log("passingers",props.passingers)
    return (
        <div className={classes.root}>
            <AppBar position="static" color="default">
                <Tabs
                    style={{ width: '100%' }}
                    value={value}
                    onChange={handleChange}
                    variant="scrollable"
                    scrollButtons="on"
                    indicatorColor="primary"
                    textColor="primary"
                    aria-label="scrollable force tabs example"
                >
                    <Tab label="Zahtevi za voznju" style={{ width: '50%' }} icon={<PhoneIcon />} {...a11yProps(0)} />
                    <Tab label="Prihvaćeni zahtevi" style={{ width: '50%' }} icon={<FavoriteIcon />} {...a11yProps(1)} />
                </Tabs>
            </AppBar>
            {/* Passingers requests start */}
            <TabPanel value={value} index={0}>
                <List>
                    {props.passingers?.users ? props.passingers?.users.map((el) => {
                        console.log("EL",el)
                        if (el.map_users_to_services.request_status_id == 3)
                            return <ListItem><Passinger passingers={el} updateRequestStatus={props.updateRequestStatus} service={props.passingers} /></ListItem>
                    }) : null}
                </List>
            </TabPanel>
            {/* Passingers requests end */}
            {/* Accepted passingers start */}
            <TabPanel value={value} index={1}>
                <List>
                    {props?.passingers?.users ? props.passingers?.users.map((el) => {
                        if (el.map_users_to_services.request_status_id == 1)
                            return <ListItem><Passinger updateRequestStatus={props.updateRequestStatus} passingers={el} service={props.passingers} /></ListItem>
                    }) : null}
                </List>
            </TabPanel>
            {/* Accepted passingers end */}
        </div>
    );
}