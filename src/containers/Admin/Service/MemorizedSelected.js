import React, { useEffect } from 'react'
import Select from '@material-ui/core/Select';
import axios from '../../../utils/axios';


const MemorizedSelect = (props) => {
    const [selectedStartLocation, setSeletectedStartLocation] = React.useState(-1);
    const [locations, setLocations] = React.useState([]);
    const fetchLocations = async () => { const result = await axios.get('/locations').then(res => res.data.result); setLocations(result) };
    useEffect(()=> {
        fetchLocations()
        setSeletectedStartLocation(props.rowData.start_desId)
    },[])




    return (
        <Select
            native
            onChange={(e) => setSeletectedStartLocation(e.target.value)}
            value={selectedStartLocation}
            style={{ margin: '0px' }}
            id="start_destination"
            defaultValue={props.rowData.start_desId}
        >
            {locations.map((el, i) => (
                <option key={el.id} value={el.id}>
                    {el.city}
                </option>
            ))}
        </Select>
    )
}
export default MemorizedSelect;