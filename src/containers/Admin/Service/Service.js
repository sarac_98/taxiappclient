import React, { useEffect } from 'react';
//My components
import Passingers from './Passingers/Passingers';
import Table from '../../../components/MaterialTable/Table';
import axios from '../../../utils/axios';
import Modal from '../../../components/Modal/Modal';
import AddButton from '../../../components/AddButton/AddButton'
import * as helpers from '../../../utils/helpers';
import MemorizedSelected from './MemorizedSelected';
import AddService from './AddService/AddService';
import ExportButton from '../../../components/ExportButton/ExportButton';
//Material UI components
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import { KeyboardTimePicker, KeyboardDatePicker } from "@material-ui/pickers";
import DateFnsUtils from '@date-io/date-fns';
//Icons
import QueryBuilderIcon from '@material-ui/icons/QueryBuilder';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
//External liberies
import moment from 'moment';
//Pure functions
const getElementById = (id) => document.getElementById(id).value
//Component Start
export default function Services() {
  //Dialog States
  const [open, setOpen] = React.useState(false);
  const [openRequests, setOpenRequests] = React.useState(false);
  //States with data from server
  const [locations, setLocations] = React.useState([]);
  const [vehicles, setVehicles] = React.useState([]);
  const [services, setServices] = React.useState([]);
  const [requestedUsers, setRequestedUsers] = React.useState([]);
  const [passingers, setPassingers] = React.useState([]);
  //Functions for handing dialog state
  const handleOpen = () => setOpen(true);
  // const [selectedStartLocation, setSeletectedStartLocation] = React.useState(-1);
  const handleClose = () => { setOpen(false); };
  const handleOpenRequests = () => setOpenRequests(true);
  const handleCloseRequests = () => setOpenRequests(false);
  //Asynchronous functions for fetching data from server
  const fetchLocations = async () => { const result = await axios.get('/locations').then(res => res.data.result); setLocations(result) };
  const fetchVehicles = async () => { const result = await axios.get('/vehicles').then(res => res.data.result); setVehicles(result) };
  const fetchPassingersForChoosenService = async (id) => {
    const serviceId = id;
    const requestId = 2;
    const result = await axios.get(`/usersToServices/usersAssignedToService/${id}?requestId=${1}`).then(res => res.data.result);

    setPassingers(result);
  };
  const [selectedStartLocation, setSeletectedStartLocation] = React.useState(-1);
  console.log("PUTNICI", passingers)

  // const fetchRequestedPassingers = async (id) => {
  //   const serviceId = id;
  //   const requestId = 1;
  //   const result = await axios.get(`/usersToServices/usersAssignedToService/${serviceId}?requestId=${requestId}`).then(res => res.data.result);
  //   setRequestedUsers(result)
  // };
  const fetchServices = async () => {
    let result = await axios.get(`services/?start_location=undefined&end_location=undefined&date=${moment(new Date()).format("YYYY-MM-DD HH:mm:ss")}`)
      .then(res => res.data.result);
    result = result.map((el, i) => {
      return {
        id: el.id,
        date: el.date,
        time: el.time,
        time_end: el.time_end,
        price: el.price,
        start_destination: el.start_des.city,
        end_destination: el.end_des.city,
        start_desId: el.start_destination,
        end_desId: el.end_destination,
        vehicle_id: el.vehicle.id,
        vehicle_model: el.vehicle.model
      }
    })
    setServices(result)
  };
  //Asynchronous functions for updaing data on server
  const updateRequestStatus = async (service_id, user_id, request_id) => {
    await axios.patch(`/usersToServices/updateRequestStatus/${service_id}/${user_id}/${request_id}`);
    fetchPassingersForChoosenService(service_id, 2)
  };
  const onRowUpdate = async (newData) => {
    console.log("VOZILO", getElementById('vehicle_id'))
    await axios.patch(`services/${newData.id}`, {
      date: newData.date,
      time: (newData.time),
      start_destination: getElementById('start_destination'),
      end_destination: getElementById('end_destination'),
      vehicle_id: getElementById('vehicle_id'),
      time_end: newData.time_end
    })
    fetchServices();
  };
  //Asynchronous functions for deleting data on server
  const onRowDelete = async (oldData) => {
    await axios.delete(`services/${oldData.id}`);
    const dataDelete = [...services];
    const index = oldData.tableData.id;
    dataDelete.splice(index, 1);
    setServices([...dataDelete]);
  };
  //Component did mount
  useEffect(() => {
    fetchServices();
    fetchLocations();
    // fetchPassingersForChoosenService()
    fetchVehicles();
    return () => { console.log("Component did unmount"); }
  }, [])

  const buttonStyle = { float: 'right', zIndex: 200, color: 'green', padding: 0, marginTop: 15, marginRight: 15, marginRightfontSize: '30px', minWidth: '30px' }

  return (
    <div >
      <AddButton handleOpen={handleOpen} title="Dodaj vožnju"></AddButton>
      <ExportButton/>

        {/* Table Component Start */}
      <Table data={services} title="Red vožnje" onRowUpdate={onRowUpdate} onRowDelete={onRowDelete} columns={
        [{
          title: 'Zahtevi', editable: 'never', cellStyle: { width: '5%' },
          render: (rowData) => {
            return (
              <AssignmentIndIcon onClick={() => {
                handleOpenRequests();
                //fetchRequestedPassingers(rowData.id);
                fetchPassingersForChoosenService(rowData.id, 2);
              }} style={{ marginLeft: '5px' }} />
            )
          }
        },
        {
          title: 'Datum', field: 'date', type: 'date', render: data => helpers.formatDate(data.date),
          editComponent: ({ value, onChange, rowData }) => {
            return (<MuiPickersUtilsProvider utils={DateFnsUtils}>
              <KeyboardDatePicker
                disableToolbar
                variant="inline"
                style={{ margin: '0px' }}
                format="dd.MM.yyyy"
                margin="normal"
                ampm={false}
                id="date-picker-inline"
                value={value}
                onChange={onChange}
                KeyboardButtonProps={{
                  "aria-label": "change date"
                }}
              />
            </MuiPickersUtilsProvider>)
          }
        },
        {
          title: 'Vreme polazka', field: 'time', type: 'datetime', render: data => moment(data.time).format("HH:mm"),
          editComponent: ({ value, onChange, rowData }) => {
            return (
              <MuiPickersUtilsProvider s utils={DateFnsUtils}>
                <KeyboardTimePicker
                  disableToolbar
                  style={{ margin: '0px' }}
                  variant="inline"
                  margin="normal"
                  ampm={false}
                  id="date-picker-inline"
                  value={value}
                  onChange={onChange}
                  keyboardIcon={<QueryBuilderIcon />}
                  KeyboardButtonProps={{
                    "aria-label": "change date"
                  }}
                />
              </MuiPickersUtilsProvider>)
          }
        },
        {
          title: 'Priblizno vreme dolazka', field: 'time_end', type: 'datetime', render: data => moment(data.time_end).format("HH:mm"),
          editComponent: ({ value, onChange, rowData }) => {
            return (
              <MuiPickersUtilsProvider s utils={DateFnsUtils}>
                <KeyboardTimePicker
                  disableToolbar
                  style={{ margin: '0px' }}
                  variant="inline"
                  margin="normal"
                  ampm={false}
                  id="date-picker-inline"
                  value={value}
                  onChange={onChange}
                  keyboardIcon={<QueryBuilderIcon />}
                  KeyboardButtonProps={{
                    "aria-label": "change date"
                  }}
                />
              </MuiPickersUtilsProvider>)
          }
        },
        {
          title: 'Mesto polazka', field: 'start_destination',
          editComponent: ({ rowData, onChange }) => {
            return (
              // <Select
              //   native
              //   onChange={(e) => setSeletectedStartLocation(e.target.value)}
              //   style={{ margin: '0px' }}
              //   id="start_destination"
              //   defaultValue={rowData.start_desId}
              // >
              //   {locations.map((el, i) => (
              //     <option key={el.id} value={el.id}>
              //       {el.city}
              //     </option>
              //   ))}
              // </Select>
              <MemorizedSelected locations={locations} rowData={rowData} selectedStartLocation={selectedStartLocation} setSeletectedStartLocation={setSeletectedStartLocation} />
            )
          }
        },
        {
          title: 'Mesto završetka', field: 'end_destination',
          editComponent: ({ rowData }) => {
            return (
              <Select
                native

                style={{ margin: '0px' }}
                id="end_destination"
                defaultValue={rowData.end_desId}
              >
                {locations.map((el, i) => (
                  <option key={el.id} value={el.id}>
                    {el.city}
                  </option>
                ))}
              </Select>
            )
          }
        },
        {
          title: 'Cena u din.', field: 'price',
        },
        {
          title: 'Vozilo', field: 'vehicle_model',
          editComponent: ({ value, onChange, rowData }) => {
            console.log("UNUTAR ID", rowData.vehicle_id)
            return (
              <Select
                native
                style={{ margin: '0px' }}
                id="vehicle_id"

                defaultValue={rowData.vehicle_id}

              >
                {vehicles.map((el, i) => (
                  <option key={el.id} value={el.id}>
                    {el.model}
                  </option>
                ))}
              </Select>
            )
          }
        },
        ]
      } />
      {/* Table Component End */}

      {/* Add Service Modal Start */}
      {open ? (
        <Modal
          isOpen={open}
          title={'Nova vožnja'}
          titleBackGround={'rgb(37,41,38)'}
          dialogContentPadding={'padding:50px 50px'}
          handleClose={handleClose}>
          <AddService locations={locations} vehicles={vehicles} fetchServices={fetchServices} handleClose={handleClose} />
        </Modal>) : null}
      {/* Add Service Modal End */}

      {/* Request Modal Start */}
      {openRequests ? <Modal title={`Putnici`}
        titleBackGround={'rgb(37,41,38)'} isOpen={openRequests} DialogContentPadding='0px' handleClose={handleCloseRequests}>
        <div style={{ width: '600px', height: '700px' }}>
          <Passingers passingers={passingers[0]?.service} requestedUsers={requestedUsers} updateRequestStatus={updateRequestStatus} />
        </div>
      </Modal>
        : null}
      {/* Request Modal End */}
    </div>
  );
}
//Component end