import React from 'react';
//My components
import axios from '../../../../utils/axios';
//Material UI components
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import { KeyboardTimePicker, KeyboardDatePicker } from "@material-ui/pickers";
import DateFnsUtils from '@date-io/date-fns';
import TextField from '@material-ui/core/TextField';

//Icons
import QueryBuilderIcon from '@material-ui/icons/QueryBuilder';
import { useDispatch } from 'react-redux';
import * as actionTypes from '../../../../Store/actions/actions';

//Constants
const intialLocationsIdsState = {
    startLocationId: 0,
    endLocationId: 0
}
// COMPONENT PROPS : locations, vehicles, fetchServices(), handleClose()
export default function AddService(props) {
    const dispatch = useDispatch();
    //States for select and date/time components
    const [currentSeletedVehicle, setCurrentSeletedVehicle] = React.useState(0);
    const [locationIds, setLocationIds] = React.useState(intialLocationsIdsState);
    const [selectedDate, setSelectedDate] = React.useState(new Date());
    const [selectedTime, setSelectedTime] = React.useState(new Date());
    const [selectedEndTime, setSelectedEndTime] = React.useState(new Date());
    const [price, setPrice] = React.useState();
    //Functions for handling select and date/time states
    const handleDateChange = (date) => setSelectedDate(date);
    const handleTimeChange = (time) => setSelectedTime(time);
    const handleEndTimeChange = (time) => setSelectedEndTime(time);
    const handleChangeLocationSelect = (e, key) => setLocationIds({ ...locationIds, [key]: Number(e.target.value) })
    //Asynchronous functions for saving data on server
    const saveServiceInDatabase = async () => {
        await axios.post('/services', {
            time: (selectedTime),
            time_end: (selectedEndTime),
            date: selectedDate,
            start_destination: locationIds.startLocationId,
            end_destination: locationIds.endLocationId,
            vehicle_id: currentSeletedVehicle,
            price: price,
        })
        props.fetchServices();
        props.handleClose();
        dispatch({
            type: actionTypes.SET_ALERT_MESSAGE,
            message: ('Uspesno ste dodali novu voznju.'),
        });
        setTimeout(() => {
            dispatch({
                type: actionTypes.SET_ALERT_MESSAGE,
                message: '',
            });
        }, 3000);

    }
    return (
        <React.Fragment>
            <div style={{ width: '440px', height: '500px', display: 'flex', flexDirection: 'column' }}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDatePicker
                        disableToolbar
                        variant="inline"
                        format="dd.MM.yyyy"
                        margin="normal"
                        id="date-picker-inline"
                        label="Date picker inline"
                        value={selectedDate}
                        onChange={handleDateChange}
                        KeyboardButtonProps={{
                            "aria-label": "change date"
                        }}
                    />
                    <InputLabel style={{ fontSize: '12px', color: 'black' }} htmlFor="assigned_to-native-simple">{'Vreme polazka'}</InputLabel>
                    <KeyboardTimePicker
                        margin="normal"
                        id="time-picker"
                        ampm={false}
                        keyboardIcon={<QueryBuilderIcon />}
                        variant="inline"
                        value={selectedTime}
                        onChange={handleTimeChange}
                        KeyboardButtonProps={{
                            "aria-label": "change time"
                        }}
                    />
                    <InputLabel style={{ fontSize: '12px', color: 'black' }} htmlFor="assigned_to-native-simple">{'Priblozno vreme dolazka'}</InputLabel>
                    <KeyboardTimePicker
                        margin="normal"
                        id="time-picker"
                        ampm={false}
                        keyboardIcon={<QueryBuilderIcon />}
                        variant="inline"
                        value={selectedDate}
                        onChange={handleEndTimeChange}
                        KeyboardButtonProps={{
                            "aria-label": "change time"
                        }}
                    />
                </MuiPickersUtilsProvider>
                <InputLabel style={{ fontSize: '12px', marginTop: '20px', color: 'black' }}
                    htmlFor="assigned_to-native-simple">{'Odaberite mesto polazka vožnje'}</InputLabel>
                <Select
                    native
                    onChange={(e) => handleChangeLocationSelect(e, 'startLocationId')}
                    value={locationIds.startLocationId}
                    label='Mesto polazka'
                >
                    <option value=""></option>
                    {props.locations.map((el, i) => (<option key={el.id} value={el.id}>{el.city}</option>
                    ))}
                </Select>
                <InputLabel style={{ fontSize: '12px', marginTop: '20px', color: 'black' }} htmlFor="assigned_to-native-simple">{'Odaberite mesto završetka vožnje'}</InputLabel>
                <Select
                    native
                    onChange={(e) => handleChangeLocationSelect(e, 'endLocationId')}
                    value={locationIds.endLocationId}
                    label='Mesto polazka'
                >
                    <option value=""></option>
                    {props.locations.map((el, i) => (<option key={el.id} value={el.id}>{el.city}</option>
                    ))}
                </Select>
                <InputLabel style={{ fontSize: '12px', marginTop: '20px', color: 'black' }} htmlFor="assigned_to-native-simple">{'Odaberite vozilo'}</InputLabel>
                <Select
                    native
                    onChange={(e) => setCurrentSeletedVehicle(e.target.value)}
                    value={currentSeletedVehicle}
                    label='Mesto polazka'
                >
                    <option value=""></option>
                    {props.vehicles.map((el, i) => (<option key={el.id} value={el.id}>{el.model + ' - broj sedišta: ' + el.seats_num}</option>
                    ))}
                </Select>
                <TextField
                    id="standard-number"
                    style={{marginTop:'5px'}}
                    label="Cena"
                    onChange={(e) => setPrice(e.target.value)}
                    value={price}
                    type="number"
                    InputLabelProps={{
                        shrink: true,
                    }}
                />
            </div>
            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                <Button type="submit" onClick={saveServiceInDatabase} variant="contained" color="primary"
                    disabled={selectedDate === '' || selectedDate === null || currentSeletedVehicle === 0 || locationIds.startLocationId === 0 || locationIds.endLocationId === 0}
                >Sačuvaj</Button>
                <Button type="submit" onClick={props.handleClose} variant="contained" color="secondary">Otkaži</Button >
            </div>
        </React.Fragment>
    )
}
