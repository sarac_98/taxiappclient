import React from 'react';
import Table from '../../../components/MaterialTable/Table';
import AddButton from '../../../components/AddButton/AddButton';

export default function Vehicle() {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div >
      <AddButton title="Dodaj vozilo"/>
      <Table title="Vozila"/>
    </div>
  );
}