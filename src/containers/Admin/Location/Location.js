import React from 'react';
import Table from '../../../components/MaterialTable/Table';
import AddButton from '../../../components/AddButton/AddButton';
import AutocompleteInput from '../../../components/AutocompleteInput/AutocompleteInput';
import axios from '../../../utils/axios';
import ListView from '../../../components/ListView/ListView';
import { Button } from '@material-ui/core';
import SaveIcon from '@material-ui/icons/Save';
import { makeStyles } from '@material-ui/core/styles';
import { CITIES as cities } from '../../../utils/citiesInSerbia';
import * as helpers from '../../../utils/helpers';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Grid from '@material-ui/core/Grid';


const useStyles = makeStyles((theme) => ({
  button: {
    margin: theme.spacing(1),
    fontSize: '18px'
  },

}));

export default function Location() {
  const classes = useStyles();
  const [locations, setLocations] = React.useState([]);
  const [item, setItem] = React.useState(null);

  const fetchLocations = async () => {
    const tempLocations = await axios.get('/locations')
      .then(response => response.data.result);
    const sortedCities = tempLocations.sort(helpers.sortArrayOfObjects("city", "asc"));
    setLocations(sortedCities);

  }

  const saveLocationToDB = async () => {
    if(item!== null) {
    const exists = await axios.get(`/locations/checkIfLocationExists?location=${item}`).then(res=> res.data.exists)
      
        if(!exists) {
          await axios.post('/locations', { city: item });
          fetchLocations();
        }
      else  console.log("postoji")
      };
    } 

  const deleteLocationFromDB = async (id) => {
    await axios.delete(`/locations/${id}`).then(() => {
      setLocations(locations.filter(l => l.id != id));
    });
  };

  React.useEffect(() => {
    fetchLocations();
  }, [])

  return (
    <div >
      <Grid container spacing={4}>
        <Grid item md={4}></Grid>
        <Grid item md={8}>
          <Grid container spacing={1}>
            <Grid item md={5}>
              {/* <AutocompleteInput items={cities} item={item} setItem={setItem} label={"Izaberite grad/opstinu"} /> */}
              <Autocomplete
                id="combo-box-demo"
                options={cities}
                getOptionLabel={(option) => option}
                value={item}
                onChange={(event,value)=> setItem(value)}
                renderInput={(params) => <TextField {...params} label="Izaberite grad" variant="outlined" />}
              />
            </Grid>
            <Grid item md={7} style={{ display: 'flex', justifyContent: 'left' }}>
              <Button
                style={{ height: '54px', margin: '0', backgroundColor: '#ffc61a', fontSize: 40, color: 'black' }}
                variant="contained"
                color="primary"
                size="small"
                className={classes.button}
                onClick={() => saveLocationToDB()}
              >+</Button>
            </Grid>

          </Grid>
        </Grid>

      </Grid>

      <Grid container spacing={4}>
        <Grid item md={3}></Grid>
        <Grid item md={6}>
          <ListView fetchLocations={fetchLocations} locations={locations} deleteLocation={deleteLocationFromDB} />
        </Grid>


      </Grid>


    </div >
  );
}

