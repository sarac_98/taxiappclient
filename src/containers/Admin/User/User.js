import React from 'react';
import Table from '../../../components/MaterialTable/Table';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import axios from '../../../utils/axios';
import Badge from '@material-ui/core/Badge';
import Avatar from '@material-ui/core/Avatar';
import { withStyles } from '@material-ui/core/styles';
const StyledBadge = withStyles((theme) => ({
  badge: {
    backgroundColor: '#44b700',
    color: '#44b700',
    boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
    '&::after': {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      borderRadius: '50%',
      animation: '$ripple 1.2s infinite ease-in-out',
      border: '1px solid currentColor',
      content: '""',
    },
  },
  '@keyframes ripple': {
    '0%': {
      transform: 'scale(.8)',
      opacity: 1,
    },
    '100%': {
      transform: 'scale(2.4)',
      opacity: 0,
    },
  },
}))(Badge);
const PhoneNumberValidation = (email) => {
  var reg = /^(\+381)?(\s|-)?0?6(([0-6]|[8-9])\d{6,8}|(77|78)\d{7}){1}$/;
  reg.test(email);
  return reg.test(email);
};




export default function User() {
  const history = useHistory();
  const user = useSelector((state) => state.user);
  const [allUsers, setAllUsers] = React.useState([]);
  const fetchUsers = async () => {
    const result = await axios.get("users/")
      .then(res => res.data.result)
    console.log(result)
    setAllUsers(result);


  }
  console.log(allUsers)
  const onRowUpdate = async (newData, oldData) => {


    await axios.patch(`users/${newData.user_id}`, {
      username: newData.username,
      password: newData.password,
      user_type: newData.user_type,
      full_name: newData.full_name,
      adress: newData.adress,
      phone_number: newData.phone_number
    })
    fetchUsers();
  }
  const onRowDelete = async (oldData) => {
    await axios.delete(`users/${oldData.user_id}`);
    const dataDelete = [...allUsers];
    const index = oldData.tableData.id;
    dataDelete.splice(index, 1);
    setAllUsers([...dataDelete]);
  };
  React.useEffect(() => {

    if (!user)
      return history.push('/');
    if (user.user_type !== 1) setTimeout(function () { return history.push(history.goBack()) }, 1);
    fetchUsers();


    return () => {
      setAllUsers([]);
    };
  }, [history, user])

  return (
    <div >
      <Table data={allUsers ? allUsers : []}
        title="Korisnici"
        onRowUpdate={onRowUpdate}
        onRowDelete={onRowDelete}
        columns={[
          {
            title: 'Avatar',
            field: 'avatar',
            editable: "never",
            render: rowData => (

              <StyledBadge
                overlap="circle"
                anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'right',
                }}
                variant={rowData.online_status === 1 ? "dot" : "standard"}
              >
                <Avatar alt="Remy Sharp" src={rowData.picture_path} />
              </StyledBadge>

            ),
          },
          {
            title: 'Korisničko Ime', field: 'username', validate: rowData =>
              rowData.username === 'pera' ? { isValid: false, helperText: 'sad je pera' } : rowData.username === '' ? { isValid: false, helperText: 'Ne moze bit prazno' } : true,
          },
          { title: 'Online Status', field: 'online_status', hidden: true },
          { title: 'Ime i Prezime', field: 'full_name', validate: rowData => rowData.full_name === '' ? { isValid: false, helperText: 'Polje ne može biti prazno' } : true, },
          { title: 'Broj telefona', field: 'phone_number', validate: rowData => !PhoneNumberValidation(rowData.phone_number) ? { isValid: false, helperText: 'Unesite validan broj telefona' } : true, },
          { title: 'Adresa', field: 'adress', validate: rowData => rowData.adress === '' ? { isValid: false, helperText: 'Polje ne može biti prazno' } : true, },
          {
            title: 'Tip Korisnika',
            field: 'user_type',
            lookup: { 0: 'User', 1: 'Admin' },
          },


        ]} />
    </div>
  );
}