import React, { useState } from 'react';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import EditIcon from '@material-ui/icons/Edit';
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import UserEditForm from '../../../components/UserEditForm/UserEditForm';
import Link from '@material-ui/core/Link';
import Modal from '../../../components/Modal/Modal';
import ChangePassword from '../../../components/ChangePassword/ChangePassword'
import axios from '../../../utils/axios';
import { useDispatch } from 'react-redux';
import * as actionTypes from '../../../Store/actions/actions';


const useStyles = makeStyles(() => ({
  root: {
    flexGrow: 1
  },
  titleContainer: {
    padding: 1
  },
  profilePicContainer: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
    padding: '20px 80px',

  },
  profilePic: {
    width: 200,
    height: 200,
    marginBottom: '30px'
  },
  userData: {
    padding: '10px 20px',
    textAlign: 'left'
  },
  userDataDetail: {
    marginTop: 1,
    marginBottom: 10
  },
  buttonsContainer: {
    padding: 20
  },
  button: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: 10
  },
  pswdbtn: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: 30,
    backgroundColor: 'rgb(199,0,0)'
  },
  deleteButton: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: 10,
    color: 'rgb(199,0,0)',
    borderColor: 'rgb(199,0,0)'
  },
}))
const BASE_URL = 'http://localhost:4000';

export default function Profile() {
  const disptach = useDispatch();
  const user = useSelector(state => state.user);
  const classes = useStyles();
  const [loggedUser, setLoggedUser] = React.useState(null);
  const [isEditable, setIsEditable] = React.useState(false);
  const [selectedFile, setSelectedFile] = useState(null);
  const fileSelectedHandler = event =>
    setSelectedFile(event.target.files[0]);
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }

  const updateProfile = async (newData, oldData) => {
    console.log("new data", newData)
    console.log(newData, oldData);
    const newUser = await axios.patch(`users/${oldData.user_id}`, {
      username: oldData.username,
      password: oldData.password,
      user_type: oldData.user_type,
      full_name: newData.full_name,
      adress: newData.adress,
      phone_number: newData.phone_number,
      description: newData.description
    }).then((res) => res.data.result)
    disptach({
      type: actionTypes.SET_USER,
      user: newUser
    })


    // fetchUsers();
  }

  const uploadPhoto = async (id) => {
    const fd = new FormData();
    fd.append('attachment', selectedFile, selectedFile.name)
    const tempPath = await axios.post('/users/upload', fd).then(res => res.data.path);

    await axios.patch(`users/${id}`, { picture_path: BASE_URL + '/' + tempPath })

    disptach({
      type: actionTypes.SET_USER,
      user: { ...user, picture_path: BASE_URL + '/' + tempPath }
    })


  }
  // const loadUser = async () => {
  //   await axios.get(`users/${user?.user_id}`).then((res) => {
  //     setLoggedUser(res.data.result)
  //   }

  //   )
  // }
  console.log("USERRRRRRRRR REDUX", user)

  React.useEffect(() => {
    // loadUser()
  }, [])

  const openEdit = () => {
    setIsEditable(true)
  }

  const closeEdit = () => {
    setIsEditable(false)
  }


  // console.log("PATHHHH", user.picture_path)
  return (
    <React.Fragment >
      {open ? (<Modal isOpen={open} handleClose={handleClose}><ChangePassword /></Modal>) : null}
      <div className={classes.root}>
        <Grid container spacing={3}>

          <Grid item xs={12}>
            <Paper className={classes.paper} className={classes.titleContainer}>
              <h2>PROFIL KORISNIKA</h2>
            </Paper>
          </Grid>

          <Grid item xs={12} sm={12} md={12} lg={3} xl={3}>
            <Paper className={classes.paper} className={classes.profilePicContainer}>
              {selectedFile ? (<Avatar alt="Remy Sharp" src={URL.createObjectURL(selectedFile)} className={classes.profilePic} />) : (
                <Avatar alt="Remy Sharp" src={user?.picture_path} className={classes.profilePic} />
              )}
              <Button
                type="button"
                fullWidth
                variant="contained"
                // color="primary"
                className={classes.button}
                component="label"
              >
                <input type="file" onChange={fileSelectedHandler} hidden />
                <EditIcon />
                <span>Izmeni fotografiju</span>
              </Button>
              <button onClick={() => uploadPhoto(user.user_id)}>Prihvati</button>
            </Paper>
          </Grid>

          {
            !isEditable ? (
              <Grid item xs={12} sm={12} md={12} lg={7} xl={7}>
                <Paper className={classes.paper} className={classes.userData}>
                  <h3>Podaci o korisniku</h3>
                  <small><i>Ime i prezime</i></small>
                  <h3 className={classes.userDataDetail}>{user?.full_name}</h3>
                  <small><i>Email</i></small>
                  <h3 className={classes.userDataDetail}>{user?.username}</h3>
                  <small><i>Telefon</i></small>
                  <h3 className={classes.userDataDetail}>{user?.phone_number}</h3>
                  <small><i>Adresa</i></small>
                  <h3 className={classes.userDataDetail}>{user?.adress}</h3>
                  <small><i>Opis</i></small>
                  <h3 className={classes.userDataDetail}>{user?.description}</h3>
                </Paper>
              </Grid>
            ) : (
              <Grid item xs={12} sm={12} md={12} lg={7} xl={7}>
                <Paper className={classes.paper} className={classes.userData}>
                  <UserEditForm user={user} closeEdit={closeEdit} update={updateProfile} />

                </Paper>
              </Grid>)
          }


          <Grid item xs={12} sm={12} md={12} lg={2} xl={2}>
            <Paper className={classes.paper} className={classes.buttonsContainer}>
              <Button
                type="button"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.button}
                onClick={openEdit}
              > <EditIcon /> <span>Izmeni profil</span>
              </Button>

              {/* <Link href="/forgotPassword" variant="body2"> */}
              <Button
                type="button"
                fullWidth
                variant="contained"
                color="secondary"
                className={classes.pswdbtn}
                onClick={handleOpen}
              > <VpnKeyIcon /> <span>Izmeni lozinku</span>
              </Button>
              {/* </Link> */}

              <Button
                type="button"
                fullWidth
                variant="outlined"
                color="secondary"
                className={classes.deleteButton}
              > <DeleteForeverIcon /> <span>Izbriši nalog</span>
              </Button>
            </Paper>
          </Grid>

        </Grid>
      </div>

    </React.Fragment>
  );
}

