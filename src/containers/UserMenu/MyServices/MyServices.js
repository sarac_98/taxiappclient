import React, { useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import axios from '../../../utils/axios';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { useSelector } from 'react-redux';
import { Fragment } from 'react';
import moment from 'moment';
const useStyles = makeStyles((theme) => ({
  root: {

    margin: 'auto',
    width: '50%',
    padding: '10px',
    borderRadius: '20px',
    marginBottom: '10px'
  },
  avatars: {
    display: 'flex',
    flexDirection: 'row',
    '& > *': {
      margin: theme.spacing(1),
    },
    formControl: {
      marginBottom: '15px'
    }
  },

}));

export default function MyServices() {
  const classes = useStyles();
  const [usersServices, setUsersServices] = useState([]);
  const [requestId, setRequestId] = useState(3);
  const user = useSelector(state => state.user);
  const [loggedUser, setLoggedUser] = useState(null);
  const fetchUserById = async (id) => {
    const temp = await axios.get(`users/${id}`).then((res) => res.data.result)
    setLoggedUser(temp)
  }
  console.log("USER", loggedUser)
  const fetchUsersServices = async (requestId) => {
    const temp = await axios.get(`usersToServices/getUsersServices/${user?.user_id}/${requestId}`).then((res) => res.data.result);
    setUsersServices(temp)
  }

  useEffect(() => {
    console.log("did mount")
    fetchUserById(user?.user_id)
    fetchUsersServices(requestId);
  }, [requestId])
  return (
    <Fragment>
      <FormControl className={classes.formControl} style={{ marginBottom: '25px' }}>
        <InputLabel htmlFor="age-native-simple">Status</InputLabel>
        <Select
          native
          value={requestId}
          onChange={(e) => setRequestId(e.target.value)}
          inputProps={{
            name: 'age',
            id: 'age-native-simple',
          }}
        >
          <option value={3}>Zahtevi za voznje</option>
          <option value={1}>Prihvacene voznje</option>
          <option value={2}>Odbijene voznje</option>



        </Select>
      </FormControl>
      {usersServices.map((el) => {
        console.log(el)
        return (


          <Paper className={classes.root} style={el.requestStatus.id === 1 ? { border: '2px solid green' } : el.requestStatus.id === 2 ? { border: '2px solid red' } : { border: '2px solid blue' }}>
            <Grid container spacing={3}>
              <Grid item xs={12} md={4} >
                <Grid container spacing={1}>
                  <Grid style={{ fontSize: '22px' }} item xs={12} md={6} >{moment(el?.service?.time).format("HH:mm")}</Grid>
                  <Grid item xs={12} md={6} style={{ fontSize: '22px' }}>{el?.service.start_des?.city}</Grid>
                </Grid>
                <Grid container spacing={1}>
                  <Grid item xs={12}  ><div style={{ height: '15px' }}></div></Grid>

                </Grid>
                <Grid container spacing={1}>
                  <Grid style={{ fontSize: '22px' }} item xs={12} md={6} >~{moment(el?.service?.time_end).format("HH:mm")}</Grid>
                  <Grid item xs={12} md={6} style={{ fontSize: '22px' }}>{el?.service?.end_des?.city}</Grid>
                </Grid>
              </Grid>
              <Grid item xs={12} md={4} >
                <Grid container spacing={1}>
                </Grid>
              </Grid>
              <Grid item xs={12} md={4} >
                <Grid container spacing={1}>
                  <Grid item xs={12} md={12} style={{ fontSize: '22px' }}><span>Cena: 550 din.</span>

                  </Grid>

                </Grid>
              </Grid>
            </Grid>
          </Paper>

        )

      })}
    </Fragment>
  )
}

