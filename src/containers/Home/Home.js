import React from 'react';
// import {useHistory} from 'react-router-dom';
// import { useSelector } from 'react-redux';
import CarouselSlider from '../../components/Carousel/Carousel'
import { makeStyles } from '@material-ui/core'
import Counters from '../../components/Counters/Counters'
import Container from '@material-ui/core/Container';
import Footer from '../../components/Footer/Footer'
import AboutCards from '../../components/AboutUs/AboutCards';
import AboutUs from '../../components/AboutUs/AboutUs';


const useStyles = makeStyles(()=>({
  container : {
    overflow : 'hidden'
  },
  about : {
    backgroundColor: 'yellow'
  }
}))

export default function Home() {

// const history = useHistory();
// const user = useSelector((state) => state.user);
const classes = useStyles();



React.useEffect(()=> {
  // if (!user) {
  //   return history.push('/login');
  // }
},[])
  return (
    <React.Fragment >
      <div className={classes.container}>
        <CarouselSlider/>
        <Container maxWidth="lg">
          <Counters/>
        </Container>
      </div>
        <AboutUs className={classes.about}/>
      <div className={classes.container}>
         <Container maxWidth="lg">
          <AboutCards/>
        </Container>
        <Footer/>
      </div>
     
    </React.Fragment>
  );
}