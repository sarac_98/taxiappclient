import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import LocalTaxiIcon from '@material-ui/icons/LocalTaxi';
const useStyles = makeStyles({
    root: {
        maxWidth: 500,
        height:300
    },
    media: {
        height: 340,
    },
});

export default function MediaCard(props) {
    const classes = useStyles();
console.log("HED",props.heading)
    return (
        <Card className={classes.root}>
             {props.icon}
            <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                    {props.title}
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                    {props.text}
                </Typography>
            </CardContent>
        </Card>
    );
}