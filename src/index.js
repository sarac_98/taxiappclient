import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { BrowserRouter } from 'react-router-dom';
import App from './App';
import jwt from 'jsonwebtoken';
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux';
import setAutherizationToken from './utils/auth';
import store from './Store/store';
import { useDispatch, useSelector } from 'react-redux';
import * as actionTypes from './Store/actions/actions';
import axios from './utils/axios';
let loggedUser = null;
const getCurrentLogedUser = async (id) => {
  return await axios.get(`/users/${id}`);
}
if (localStorage.jwtToken) {
  //console.log(jwt.decode(localStorage.jwtToken).user);
  setAutherizationToken(localStorage.jwtToken);


  (async function main() {
    const returnedData = await getCurrentLogedUser(jwt.decode(localStorage.jwtToken).user.user_id).then((res) => res.data.result);
    store.dispatch({
      type: actionTypes.SET_USER,
      user: returnedData,
    })
  })();




}
ReactDOM.render(

  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,

  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
