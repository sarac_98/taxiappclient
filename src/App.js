import {React,useEffect} from 'react';
import logo from './logo.svg';
import axios from 'axios';
import './App.css';
import { createStore } from 'redux';
import jwt from 'jsonwebtoken';
import Main from './containers/Main/Main';
import store from './Store/store';
import Alert from  './components/AlertBox/Alert';
import * as actionTypes from './Store/actions/actions';

function App() {
 
  return (
    <div className="App">
      <Alert/>
      <Main />
     
    </div>
  );
}

export default App;
