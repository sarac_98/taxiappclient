export const SET_USER = 'SET_USER';
export const SET_TOKEN = 'SET_TOKEN';
export const IS_LOADING = 'IS_LOADING';
export const SET_PICTURE = 'SET_PICTURE';
export const SET_ALERT_MESSAGE = "SET_ALERT_MESSAGE";
export const SET_MSG_TYPE = "SET_MSG_TYPE";