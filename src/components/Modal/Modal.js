import React, { useEffect } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});

export default function Modal(props) {

  return (
    <Dialog
      open={props.isOpen}
      TransitionComponent={Transition}
      keepMounted
      onClose={props.handleClose}
      aria-labelledby="alert-dialog-slide-title"
      aria-describedby="alert-dialog-slide-description"
    >
      <DialogTitle style={{ backgroundColor: props.titleBackGround, textAlign: 'center', color: 'white', fontWeight: 'bold' }} id="alert-dialog-slide-title">
        {props.title}
      </DialogTitle>
      <DialogContent style={{ padding: props.DialogContentPadding }}>
        {props.children}
      </DialogContent>

      <DialogActions style={{
        display: 'flex', flexDirection: 'row', justifyContent: 'flex-end',
      }}>
        {props.submitButton}
        {props.cancelButton}
      </DialogActions>
    </Dialog>

  );
}
