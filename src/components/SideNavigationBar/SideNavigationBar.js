import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import { useSelector } from "react-redux";
import Collapse from '@material-ui/core/Collapse';
import Hidden from '@material-ui/core/Hidden';
import { useHistory} from 'react-router-dom';
import HomeIcon from '@material-ui/icons/Home';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import LocalTaxiIcon from '@material-ui/icons/LocalTaxi';
import SecurityIcon from '@material-ui/icons/Security';
import AssignmentIcon from '@material-ui/icons/Assignment';
import PeopleIcon from '@material-ui/icons/People';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import DirectionsCarIcon from '@material-ui/icons/DirectionsCar';
import PaymentIcon from '@material-ui/icons/Payment';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import logo from '../../logo.png';

const useStyles = makeStyles((theme) => ({
  logo: {
    position: 'absolute',
    left: 20,
   
    [theme.breakpoints.down('sm')]: {
      marginLeft: '40px',
      fontSize: '17px'
    },
    textDecoration: 'none',
    color: 'inherit'
  },
  logoImg : {
    width: '150px',
    marginTop: '5px',
  },
  root: {
    display: 'flex',
    zIndex: '0',
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
      zIndex: '0',
    },
  },
  links: {
    textDecoration: 'none',
    color: 'white',
   

  },
  toolbar: theme.mixins.toolbar,
  // necessary for content to be below app bar
  drawerPaper: {
    width: drawerWidth,
    backgroundColor: '#484848'
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  
  nested: {
    paddingLeft: theme.spacing(4),
    color:'white'
  },

}));
const drawerWidth = 240;
const SideNavigationBar = (props) => {
  const user = useSelector(state => state.user);
  const history = useHistory();
  const { window } = props;
  const classes = useStyles();
  const theme = useTheme();
  const drawer = (
    <div>
      <div className={classes.toolbar} >
      <Link to="/" className={classes.logo}><img src={logo} className={classes.logoImg}/></Link>
      </div>
      <Divider />
      <List>
        <Link to="/" className={classes.links}>
          <ListItem button onClick={() => props.setMobileOpen(false)}>
            <ListItemIcon><HomeIcon style={{ color: 'white' }} /></ListItemIcon>
            <ListItemText primary={"Početna stranica"} />
          </ListItem>
        </Link>
        <Link to="/orders" className={classes.links}>
          <ListItem button onClick={() => props.setMobileOpen(false)}>
            <ListItemIcon><LocalTaxiIcon style={{ color: 'white' }} /></ListItemIcon>
            <ListItemText primary={"Zakazivanje vožnje"} />
          </ListItem>
        </Link>
        {user?.user_type === 1 ? (<React.Fragment><ListItem className={classes.links} button onClick={props.handleOpenAdminPanel}>
          <ListItemIcon>
          <SecurityIcon style={{color:'white'}}/>
          </ListItemIcon>
          <ListItemText primary="Admin panel" />
          {props.isAdminOpen ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={props.isAdminOpen} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <Link className={classes.links} to="/admin/services">
            <ListItem className={classes.nested}button >
              <ListItemIcon>
              <AssignmentIcon style={{color:'white'}}/>
              </ListItemIcon>
              <ListItemText primary="Red vožnje" />
            </ListItem>
            </Link>
            <Link className={classes.links} to="/admin/users">
            <ListItem className={classes.nested}button >
              <ListItemIcon>
              <PeopleIcon style={{color:'white'}}/>
              </ListItemIcon>
              <ListItemText primary="Korisnici" />
            </ListItem>
            </Link>
            <Link className={classes.links} to="/admin/locations">
            <ListItem className={classes.nested}button >
              <ListItemIcon>
              <LocationOnIcon style={{color:'white'}}/>
              </ListItemIcon>
              <ListItemText primary="Lokacije" />
            </ListItem>
            </Link>
            <Link className={classes.links} to="/admin/vehicles">
            <ListItem className={classes.nested}button >
              <ListItemIcon>
              <DirectionsCarIcon style={{color:'white'}}/>
              </ListItemIcon>
              <ListItemText primary="Vozila" />
            </ListItem>
            </Link>
            <Link className={classes.links} to="/admin/ads">
            <ListItem className={classes.nested}button >
              <ListItemIcon>
              <PaymentIcon style={{color:'white'}}/>
              </ListItemIcon>
              <ListItemText primary="Reklame" />
            </ListItem>
            </Link>
          </List>
        </Collapse></React.Fragment>) : null}
        
      </List>
    </div>
  );
  const container = window !== undefined ? () => window().document.body : undefined;
  return (
    history.location.pathname !== '/login' ? (
      <nav className={classes.drawer} aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={props.mobileOpen}
            onClose={props.handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
    ) : ''
  )
}
export default withRouter(SideNavigationBar);