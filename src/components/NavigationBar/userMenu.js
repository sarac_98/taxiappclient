import React, { useEffect } from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Avatar from '@material-ui/core/Avatar'
import { useSelector } from "react-redux";
import { makeStyles } from '@material-ui/core';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import PersonIcon from '@material-ui/icons/Person';
import PowerSettingsNewIcon from '@material-ui/icons/PowerSettingsNew';
import { Link } from 'react-router-dom';
import axios from '../../utils/axios';


const useStyles = makeStyles((theme) => ({
  menu: {
    marginTop: 45
  },
  button: {
    backgroundColor: "rgba(0, 0, 0, 0.77); !important",
    padding: '5px 20px',
    display: "flex",
    [theme.breakpoints.down("xs")]: {
      backgroundColor: "rgba(0, 0, 0, 0); !important",
      border: '0px solid black',
      justifyContent: "flex-end",
      paddingRight: 0
    },
  },
  userName: {
    color: "white",
    marginLeft: 10,
    [theme.breakpoints.down("xs")]: {
      display: 'none'
    },
  },
  link: {
    color: 'inherit',
    textDecoration: 'none'
  }

}));

export default function SimpleMenu(props) {
  const user = useSelector(state => state.user);

  const classes = useStyles();

  return (
    <div>

      <Button
        variant="outlined"
        color="default"
        className={classes.button}
        aria-controls="simple-menu"
        aria-haspopup="true"
        onClick={props.handleClickUserMenu}>
        <Avatar alt="Remy Sharp" src={user.picture_path ? user.picture_path : null} />
        <div className={classes.userName}>{user.full_name}</div>
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={props.userMenu}
        keepMounted
        open={Boolean(props.userMenu)}
        onClose={props.handleCloseUserMenu}
        className={classes.menu}
      >
        <Link to="/profile" className={classes.link}>
          <MenuItem onClick={props.handleCloseUserMenu}>

            <ListItemIcon>
              <PersonIcon />
            </ListItemIcon>
            Pogledaj profil

          </MenuItem>
        </Link>
        <Link to="/myservices" className={classes.link}>
          <MenuItem onClick={props.handleCloseUserMenu}>

            <ListItemIcon>
              <PersonIcon />
            </ListItemIcon>
            Moje vožnje

          </MenuItem>
        </Link>
        <MenuItem onClick={() => { props.handleCloseUserMenu(); props.logOut(); }}>
          <ListItemIcon>
            <PowerSettingsNewIcon />
          </ListItemIcon>
          Odjavi se
        </MenuItem>
      </Menu>
    </div>
  );
}