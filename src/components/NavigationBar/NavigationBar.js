import React, { useEffect } from 'react';
import AppBar from '@material-ui/core/AppBar';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import { useSelector } from "react-redux";
import Button from '@material-ui/core/Button';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import UserMenu from './userMenu';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import LoginNav from './LoginNav';
import Login from '../../components/Login/Login';
import Signup from '../../components/Signup/Signup';
import Modal from '../Modal/Modal';
import { Link, withRouter } from 'react-router-dom';
import logo from '../../logo.png';
import saki from '../../saki.png';
import axios from '../../utils/axios';

const useStyles = makeStyles((theme) => ({
  logo: {
    position: 'absolute',
    left: 15,
    [theme.breakpoints.down('sm')]: {
      marginLeft: '40px',
      fontSize: '17px'
    },
    textDecoration: 'none',
    color: 'inherit'
  },
  logoImg: {
    width: '150px',
    marginTop: '5px',
  },
  appBar: {
    zIndex: '5',
    [theme.breakpoints.up('sm')]: {
      width: `100%`,
      marginLeft: drawerWidth,

    },
    backgroundColor: 'rgb(35, 35, 35)'
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  loginNav: {
    backgroundColor: 'blue',
    display: 'flex',
    flexDirection: 'row',
    float: 'right'
  }
}));
const drawerWidth = 240;
export default function NavigationBar(props) {
  const classes = useStyles();
  const user = useSelector(state => state.user);

  const matches = useMediaQuery('(min-width:500px)');
  return (
    <AppBar position="fixed" className={classes.appBar}>
      <Toolbar >
        <Link to="/" className={classes.logo}><img src={logo} className={classes.logoImg} /></Link>
        {(
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={props.handleDrawerToggle}
            className={classes.menuButton}>
            <MenuIcon />
          </IconButton>
        )}
        {user ? (<div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-end', width: '100%' }} >


          <UserMenu
            loggedUser={props.loggedUser}
            userMenu={props.userMenu}
            logOut={props.logOut}
            setUserMenu={props.setUserMenu}
            handleClickUserMenu={props.handleClickUserMenu}
            handleCloseUserMenu={props.handleCloseUserMenu} /></div>
        ) :
          <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-end', width: '100%', color: 'gainsboro' }} >
            <Button style={{ color: 'gainsboro' }} onClick={() => { props.handleOpenLoginModal() }} >Prijava</Button>
            <Button style={{ color: 'gainsboro' }} onClick={() => { props.handleOpenRegistrationModal() }} >Registracija</Button>
          </div>}

      </Toolbar>
      {props.openLogin ? (<Modal isOpen={props.openLogin}
        handleClose={props.handleCloseLoginModal}>
        <Login handleCloseLoginModal={props.handleCloseLoginModal}
          handleOpenRegistrationModal={props.handleOpenRegistrationModal} />
      </Modal>) : null}

      {props.openRegistration ? (<Modal isOpen={props.openRegistration}
        handleClose={props.handleCloseRegistrationModal}>
        <Signup handleCloseRegistrationModal={props.handleCloseRegistrationModal}
          handleOpenLoginModal={props.handleOpenLoginModal} />
      </Modal>) : null}

    </AppBar>
  )
}

