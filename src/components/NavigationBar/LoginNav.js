import React from 'react';
import Button from '@material-ui/core/Button';
import Modal from '../Modal/Modal';
import Login from '../Login/Login';
export default function LoginNav(params) {

  const [open,setOpen] = React.useState(false);
  const handleOpen = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }
    return (
        <>
            <Button onClick={()=> handleOpen()}  variant="contained" >Prijava</Button>
            {open }
        </>
    )
}