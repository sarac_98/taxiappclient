import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import jwt from 'jsonwebtoken';
import setAutherizationToken from '../../utils/auth';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import {useHistory} from 'react-router-dom';
import { useForm } from 'react-hook-form'
import store from '../../Store/store';
import * as actionTypes from '../../Store/actions/actions';
import axios from '../../utils/axios';
const useStyles = makeStyles((theme) => ({
  paper: {
    // marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    paddingBottom: 50
  },
  icon: {
    color: 'black'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: '#ffc61a',
    
  },
  form: {
    width: '100%', 
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: '#ffc61a',
    color: 'black',
    // fontWeight:  'bold',
    '&:hover': {
      backgroundColor: '#c79600',
      color: 'white',
    }
  },
  register : {
    marginTop : 20
  }
}));
export default function Login(props) {
  const classes = useStyles();
  const { register, handleSubmit} = useForm();
  const history = useHistory();

  const submitHandler = async (data) => {
    const token = await axios
      .post('users/login', {
        username: data.email,
        password: data.password
      })
      .then((res) => {
        console.log(res.data)
        return res.data.token;
      })
      .catch((err) => {
        console.log(err);
      });
    if (!token) {
      console.log("greska");
      return;
    }
    console.log("TOKEN",token)
    localStorage.setItem('jwtToken', token);
    setAutherizationToken(token)
    store.dispatch({
      type: actionTypes.SET_USER,
      user: jwt.decode(localStorage.jwtToken).user,
    });
   props.handleCloseLoginModal();
    //window.location.reload(true);
  };

  const openSignUp = ()=> {
        props.handleCloseLoginModal()
        props.handleOpenRegistrationModal() 
  }
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon className={classes.icon}/>
        </Avatar>
        <Typography component="h1" variant="h5">
          Prijava
        </Typography>
        <form className={classes.form} onSubmit={handleSubmit(submitHandler)} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            inputRef={register}
            label="Email Adresa"
            name="email"
            autoComplete="email"
            autoFocus
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            inputRef={register}
            label="Lozinka"
            type="password"
            id="password"
            autoComplete="current-password"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Prijavi se
          </Button>
          <Grid container>
            <Grid item >
              <Link href="/forgotPassword" variant="body2">
                Zaboravili ste lozinku ?
              </Link>
            </Grid>
          </Grid>
          {/* <Link href="/signup"> */}
            <Button
              type="button"
              fullWidth
              variant="outlined"
              color="primary"
              className={classes.register}
              onClick={openSignUp}
            >
              Registracija
            </Button>
          {/* </Link> */}
        </form>
      </div>
    </Container>
  );
}