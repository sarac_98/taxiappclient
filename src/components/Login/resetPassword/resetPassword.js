import React, { useEffect } from 'react';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import axios from '../../../utils/axios';
import { useForm } from 'react-hook-form';
import { useParams } from "react-router";
import { useSelector } from "react-redux";
import { makeStyles } from '@material-ui/core/styles';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}
const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
    },
}));

const ResetPassword = (props) => {
    const user = useSelector(state => state.user);
    const { register, handleSubmit } = useForm();
    const [error, setError] = React.useState('');
    let { token } = useParams();
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const handleClick = () => {
        setOpen(true);
    };
    useEffect(() => {
        if(user){
            console.log("p")
        }
        console.log(token)
      }, [])
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpen(false);
    };
    const submitForm = async (data) => {
        let hasError = false;
        if (data.password === '' || data.repeatPassword === '') {
            setError('Molimo vas popunite sva polja!');
            hasError = true;
        }
        if (data.password.length <= 5 && data.password.length >= 1) {
            setError('Vaša lozinka je previse kratka! Pokušajte ponovo');
            hasError = true;
        }
        if (data.password !== data.repeatPassword) {
            setError('Lozinke se ne poklapaju!');
            hasError = true;
        }
        if (hasError) {
            return;
        }
        const result = await axios.patch(`users/resetPassword/${token}`, {
            password: data.password
        }).catch(err => console.log(err))
        if(result){
            setError('')
            handleClick();
            setTimeout(function () { props.history.push('/home') }, 1000);
        }
        
    }
    return (
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', marginTop: '150px' }}>
            <form onSubmit={handleSubmit(submitForm)}>
                <Paper elevation={3} style={{ display: 'flex', height: '400px', flexDirection: 'column', width: '500px', padding: '15px' }}>
                    <h2>Restartovanje lozinke </h2>
                    <TextField inputRef={register} type='password' name='password' style={{ marginTop: '20px' }}
                        id="outlined-basic" label="lozinka" variant="outlined" />
                    <TextField inputRef={register} type='password' name='repeatPassword' style={{ marginTop: '20px' }}
                        id="outlined-basic" label="ponovite lozinku" variant="outlined" />
                    <span style={{ color: 'red', fontWeight: 'bold' }}>{error}</span>
                    <Button onClick={() => document.getElementById("hidden").click()} style={{ marginTop: '20px' }}
                        variant="contained" color="primary">Pošalji</Button>
                    <input id="hidden" hidden type="submit" />
                </Paper>
            </form>
            <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success">
                    Uspešno ste promenili vašu lozinku!
        </Alert>
            </Snackbar>
        </div>
    )
}
export default ResetPassword;