import React,{useEffect} from 'react';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import axios from '../../../utils/axios';
import { useForm } from 'react-hook-form';
import { useSelector } from "react-redux";

const ForgotPassword = (props) => {
    const user = useSelector(state => state.user);
console.log("rendering Forgotpassword")
    const { register, handleSubmit } = useForm();
    const [existingMessage, setExistingMessage] = React.useState('');
    useEffect(() => {
        if(user){
            props.history.push('/home')
        }
      }, [])
    const submitForm = async (data) => {
        const result = await axios.post('users/forgotPassword', {
            email: data.email
        }).catch(error=> console.log(error))
        if(result){
            setExistingMessage('Poslali smo Vam link za restartovanje lozinke! Proverite vaš email');
      
        }
        else {
       
            setExistingMessage('Korisnik nije pronadjen u bazi!');
        }
    }
    return (
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', marginTop: '150px' }}>
            <form onSubmit={handleSubmit(submitForm)}>
                <Paper elevation={3} style={{ display: 'flex', height: '400px', flexDirection: 'column', width: '500px', padding: '15px' }}>
                    <h2>Zaboravili ste lozinku? </h2>
                    <h4>Molimo Vas da unesete vašu email adresu sa kojom ste se registriovali na ovaj profil</h4>
                    <span style={{ color: 'gray' }}>Dobićete link na email-u za podeševanje vaše nove lozinke</span><br />
                    <TextField inputRef={register()} name='email' style={{ marginTop: '20px' }}
                        id="outlined-basic" label="Email" variant="outlined" />
                    <span>{existingMessage}</span>
                    <Button onClick={() => { document.getElementById("hidden").click(); }} style={{ marginTop: '20px' }}
                        variant="contained" color="primary">Pošalji</Button>
                    <input id="hidden" hidden type="submit" />
                </Paper>
            </form>
        </div>
    )
}
export default ForgotPassword;