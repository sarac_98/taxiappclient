import React, { useRef, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import FolderIcon from '@material-ui/icons/Folder';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import TextField from '@material-ui/core/TextField';
import AccountCircle from '@material-ui/icons/AccountCircle';
import InputAdornment from '@material-ui/core/InputAdornment';
import SearchIcon from '@material-ui/icons/Search';
import Modal from '../Modal/Modal';
import AddButton from '../AddButton/AddButton';
import { Button } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        maxWidth: 752,
    },
    demo: {
        backgroundColor: theme.palette.background.paper
    },
    title: {
        margin: theme.spacing(4, 0, 2),
    },
}));

const ListView = (props) => {
    const classes = useStyles();
    const { locations } = props;
    const { deleteLocation } = props;
    const { fetchLocations } = props;
    const [search, setSearch] = useState('');
    const [open, setOpen] = useState(false);
    const [choosenCityId, setChoosenCityId] = useState(null);

    const handleOpenModal = async (id) => {
        setOpen(true);
        setChoosenCityId(id);
    }
    // console.log('render listView component')
    const handleCloseModal = async () => {
        setOpen(false);
       // setChoosenCityId(null);
        // fetchLocations();
    }

    const okButtonEventHandler = async (event) => {
        deleteLocation(choosenCityId);
        handleCloseModal();
    }

    const okButton = <Button style={{ background: 'green', color: 'white' }} onClick={okButtonEventHandler}>Da</Button>;
    const cancelButton = <Button style={{ background: 'red', color: 'white' }} onClick={handleCloseModal}>Odustani</Button>;
console.log(locations)
    return (
        <div className={classes.root}>
            {/* <Grid item xs={12} md={6}> */}
            <Typography variant="h6" className={classes.title}>
                Lokacije
          </Typography>
            <div style={{ display: 'flex', justifyContent: 'left' }}>
                <TextField
                    style={{ width: '250px', marginBottom: '10px' }}
                    className={classes.margin}
                    id="input-with-icon-textfield"
                    placeholder="Pretraga"
                    value={search}
                    onChange={(event) => setSearch(event.target.value)}
                    InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                                <SearchIcon />
                            </InputAdornment>
                        ),
                    }}
                />
            </div>

            <div className={classes.demo}>
                {/* <div style={{ overflowY: 'hidden' }}> */}
                <List style={{ minHeight: '450px', overflowY: 'auto' }}>
                    {locations.map((el) => {
                        if (!el.city.trim().toLowerCase().includes(search.trim().toLowerCase()) && search.trim() !== null) return;
                        return (
                            <ListItem key={el.id} divider >
                                <ListItemText
                                    primary={el.city}
                                />
                                <ListItemSecondaryAction>
                                    {/* <div id={el.id} onClick={(event) => deleteLocation(el.id)}> */}
                                    <div id={el.id} onClick={(event) => handleOpenModal(el.id)}>
                                        <IconButton edge="end" aria-label="delete">
                                            <DeleteIcon />
                                        </IconButton>
                                    </div>
                                    {/* <IconButton edge="end" aria-label="edit">
                                            <EditIcon />
                                        </IconButton> */}
                                </ListItemSecondaryAction>
                            </ListItem>)
                    }
                    )}
                </List>
            </div>

            {open ? (<Modal isOpen={open}

                handleClose={handleCloseModal} submitButton={okButton} cancelButton={cancelButton}>
                <h2>Da li ste sigurni da želite da izbrišete izabranu lokaciju?</h2>
            </Modal>) : null}


        </div >
    );
}

export default ListView;