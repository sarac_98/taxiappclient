import React, { useState } from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import MailOutline from '@material-ui/icons/MailOutline';


const useStyles = makeStyles((theme) => ({
    title : {
        margin: '30px 0px',
        fontSize: '1.4em'
    },
    form : {
        width: 400,
        color : 'white',
        textAlign: 'left'
    },
    label : {
        fontStyle : 'italic'
    },
    input : {
        width: '100%',
        height: 50,
        color: 'white',
        backgroundColor: 'inherit',
        border: '1px solid white',
        borderRadius: 5,
        padding: '15px',
        margin: "3px 0px 10px",
        fontSize: 18,
        "&:focus": {
                outline: "none",
                border: '1px solid yellow'
            }
    },
    button : {
        backgroundColor: 'inherit',
        width: 130,
        height: 50,
        borderRadius: 5,
        border: '1px solid white',
        fontSize: 17,
        color: 'white',
        cursor: 'pointer',
         "&:hover": {
                outline: "none",
                border: '1px solid #ffc61a',
                color: '#ffc61a'
            }
    },
    buttonText: {
        position: "relative",
        top: "-5px"
    }
}));


export default function Signup(props) {

    const errorColor = '#ff6767';

    const [visitorName, setVisitorName] = useState('');
    const [visitorEmail, setVisitorEmail] = useState('');
    const [visitorMessage, setVisitorMessage] = useState('');

    const [nameLabel, setNameLabel] = useState('Ime i prezime');
    const [emailLabel, setEmailLabel] = useState('Email adresa');
    const [messageLabel, setMessageLabel] = useState('Vaša poruka');

    const [nameError, setNameError] = useState(false);
    const [emailError, setEmailError] = useState(false);
    const [messageError, setMessageError] = useState(false);


    const [email, setEmail] = useState('');

    const formSubmit = () => {
        if (visitorName.trim().length === 0) {
            setNameLabel('Ime je obavezno za unos');
            setNameError(true)
        }
        if (visitorEmail.trim().length == 0) {
            setEmailLabel('Email adresa je obavezna za unos');
            setEmailError(true)
        }
        if (visitorMessage.trim().length == 0) {
            setMessageLabel('Poruka je obavezna za unos');
            setMessageError(true)
        }


        
    }

    const classes = useStyles();
    return (
        <form className={classes.form}> 
            <h3 className={classes.title}>Kontaktirajte nas</h3>
            <div>
                <label className={classes.label} style={{ color: nameError ? errorColor : 'white' }} for="name"> {nameLabel} </label>
                <input className={classes.input} style={{ borderColor: nameError ? errorColor : 'white' }} 
                    type="text" name="name" id="name" onChange={ e => { 
                        setVisitorName(e.target.value); 
                        setNameError(false);
                        setNameLabel('Ime i prezime') }} />
            </div>
            <div>
                <label className={classes.label} style={{ color: emailError ? errorColor : 'white' }} for="email"> {emailLabel} </label>
                <input className={classes.input} style={{ borderColor: emailError ? errorColor : 'white' }} 
                    type="text" name="email" id="email" onChange={e => setVisitorEmail(e.target.value)} />
            </div>
            <div>
                <label className={classes.label} style={{ color: emailError ? errorColor : 'white' }} for="message"> {messageLabel} </label>
                <textarea className={classes.input} style={{ height: 100, borderColor: messageError ? errorColor : 'white' }} 
                    type="text" name="message" id="message" onChange={e => setVisitorMessage(e.target.value)}/>
            </div>
            <button className={classes.button} type="button" onClick={formSubmit}> <MailOutline/> <span className={classes.buttonText}>POŠALJI</span></button>
        </form>
  );
}