import React, { useEffect, useRef } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import jwt from 'jsonwebtoken';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import {useHistory} from 'react-router-dom';
import { useForm } from 'react-hook-form'
import store from '../../Store/store';
import * as actionTypes from '../../Store/actions/actions';
import axios from '../../utils/axios';
const useStyles = makeStyles((theme) => ({
  paper: {
    // marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    paddingBottom: 50
  },
  icon: {
    
    color: 'black'
  },
  avatar: {
   
    margin: theme.spacing(1),
     backgroundColor: '#ffc61a',
  },
  form: {
    width: '100%', 
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: '#ffc61a',
    color: 'black',
    // fontWeight:  'bold',
    '&:hover': {
      backgroundColor: '#c79600',
      color: 'white',
    }
  },
  error: {
    color: 'red',
    "&::before": {
                content: `'⚠ '`,
                display: "inline",
               
            }
  }
}));
export default function Signup(props) {
  const classes = useStyles();
  const { register, handleSubmit, watch, errors, clearErrors} = useForm();
  const history = useHistory();
  useEffect(()=> {
    return () => {
      clearErrors()
    }
  },[])

  const password = useRef({});
  password.current = watch("password", "");

  const submitHandler = async (data) => {
      const token = await axios
      .post('users/signup', {
        username: data.email,
        password: data.password,
        full_name: data.full_name,
        adress: data.adress,
        phone_number: data.phone_number,
        user_type: 0
      })
      .then((res) => {
        console.log(res.data)
        props.handleCloseRegistrationModal();
        props.handleOpenLoginModal();
      
      })
      .catch((err) => {
        console.log(err);
      });
  };



  console.log(errors);
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon className={classes.icon}/>
        </Avatar>
        <Typography component="h1" variant="h5">
          Registracija
        </Typography>
        <form className={classes.form} onSubmit={handleSubmit(submitHandler)} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="full_name"
            inputRef={register({
              pattern: { 
                value: /^[A-Za-z]/, 
                message: "Ime ne sme sadrzati brojeve"}, 
                required: { 
                  value: true, 
                  message: "Polje je obavezno za unos"}
                })}
            label="Ime i prezime"
            name="full_name"
          />

          {errors.full_name && <span className={classes.error}>{errors.full_name.message}</span>}

          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            inputRef={register({
              pattern: {
                value: /^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/,
                message: "Email adresa nije pravilno uneta"
              }, 
              required: {
                value: true,
                message: "Email adresa je obavezna za unos"}
              })}
            label="Email"
            name="email"
            autoComplete="email"
          />
          
          {errors.email && <span className={classes.error}>{errors.email.message}</span>}

          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            inputRef={register({
                required: "Lozinka je obavezna za unos",
                pattern: {
                  value: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/,
                  message: "Lozinka mora sadrzati bar jedan broj i jedno slovo"
                },
                minLength: {
                  value: 8,
                  message: "Lozinka mora sadrzati minimum 8 karaktera"
                }
              })}
            label="Lozinka"
            type="password"
            id="password"
          />
          {errors.password && <span className={classes.error}>{errors.password.message}</span>}

          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password_repeat"
            inputRef={register({
          validate: value =>
            value === password.current || "Lozinke se ne podudaraju"
        })}
            label="Potvrda lozinke"
            type="password"
            id="password_repeat"
            
          />
          {errors.password_repeat && <span className={classes.error}>{errors.password_repeat.message}</span>}
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            inputRef={register({
              required : "Broj telefona je obavezan za unos",
              pattern: {
                value: /^(\+381)?(\s|-)?0?6(([0-6]|[8-9])\d{6,8}|(77|78)\d{7}){1}$/,
                message: "Broj telefona nije pravilno unet"
              }
            })}
            label="Broj mobilnog telefona"
            name="phone_number"
            autoComplete="email"
          />
          {errors.phone_number && <span className={classes.error}>{errors.phone_number.message}</span>}
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            inputRef={register}
            label="Adresa"
            name="adress"
            autoComplete="email"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Registruj nalog
          </Button>
        </form>
      </div>
    </Container>
  );
}