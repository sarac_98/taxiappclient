import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles({
    inputStyle: {
        width: '270px',
        paddingBottom: '10px'

    }
});

const AutocompleteInput = (props) => {
    const { items } = props;
    const { item, setItem } = props;
    const classes = useStyles();


    const setItemEventHandler = (event, newValue) => setItem(newValue);

    return (
        <Autocomplete
            id="combo-box-demo"
            options={props.items}
            getOptionLabel={(item) => item}
            value={item}
            onChange={setItemEventHandler}
            className={classes.inputStyle}
            onInputChange={setItemEventHandler}
            renderInput={(params) => <TextField {...params} label={props.label} variant="outlined" />}
        />
    );
}

export default AutocompleteInput;