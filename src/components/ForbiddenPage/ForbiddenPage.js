import React from 'react';

const ForbiddenPage = () => {
    const [timer , setTimer ] = React.useState(5)
    React.useEffect(() => {
          setInterval(function(){ setTimer(prevState=> prevState-1) }, 1000);
      }, [])
    return (
    <div className="container">
        <img style={{width:'1200px', height:'680px'}} src="/img/zabrana.png" alt="fireSpot" />
      <h2>Bićete vraćeni na predhodnu stranicu za :</h2>
      <h2>{timer}</h2>
    </div>
    )
}
console.log("Cao")
export default ForbiddenPage;