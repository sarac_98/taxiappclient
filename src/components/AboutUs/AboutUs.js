import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import { PhoneEnabled, PlayCircleFilledWhite } from '@material-ui/icons';
import { Container, Grid } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(2),
    backgroundColor: '#ffc61a',
    // backgroundImage: `linear-gradient(yellow, #ffc61a)`,
    
    marginTop: 30,
    marginBottom: 50,
    paddingBottom: 40
  },
  paperAbout: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: '#282828',
    boxShadow: 'none',
    backgroundColor: 'inherit',
    paddingTop: 0,
    marginTop: -30
  },
  number: {
    fontSize: '5em'
  },
  contactTel: {
    color: '#282828',
    padding: '40px 0px',
    fontSize: '1.7em',
    position: 'relative',
    boxShadow: 'none',
    backgroundColor: 'inherit'
  },
  phoneIcon: {
    position: 'absolute',
    fontSize: 200,
    bottom: 50,
    right: 40,
    color: '#282828k',
  }
}));

export default function Counters() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
         <Container maxWidth="lg">
             <Grid > 
                <Paper className={classes.contactTel}>
                    <PhoneEnabled className={classes.phoneIcon} />
                    <h1>062/580-500</h1>
                    <h1>069/199-80-50</h1>
                </Paper>
                <Paper className={classes.paperAbout}>
                    <h2>Taksi Šarac je osnovan 2009 godine kao linijski taksi koji saobraća na relaciji Novi Bečej - Novi Sad.
                    Već 12 godina gradimo poverenje klijenata komfornom i bezbednom vožnjom.</h2>
                </Paper>
             </Grid>
         </Container>
          
    </div>
  );
}