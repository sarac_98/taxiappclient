import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import PetsIcon from '@material-ui/icons/Pets';
import LocalTaxiIcon from '@material-ui/icons/LocalTaxi';
import FlightTakeoff from '@material-ui/icons/FlightTakeoff';
import AirportShuttle from '@material-ui/icons/AirportShuttle';
import MediaCard from '../../containers/Home/MediaCard';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(2),
  },
  icon: {
    color: '#ffc61a',
    width: 100,
    height: 100
  }
}));

export default function AboutCards() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={3}>
          <MediaCard
            icon={<LocalTaxiIcon className={classes.icon} />}
            title={"REDOVNA VOŽNJA"}
            text={"Rapolažemo sa preko 500 klimatizovanih, udobnih i komfornih vozila. Prepustite se i uživajte u sigurnoj vožnji sa našim ljubaznim profesionalcima"}
          />
        </Grid>
        <Grid item xs={3}>
          <MediaCard
            icon={<PetsIcon className={classes.icon} />}
            title={"PET FRIENDLY"}
            text={"Raspolažemo sa dva klimatizovana vozila, koja su specijalno prilagođena vašim ljubimcima, jer oni su deo naših porodica. Prevoz kućnih ljubimaca sa nama je siguran, udoban, besplatan i bez stresa. "}
          />
        </Grid>
        <Grid item xs={3}>
          <MediaCard
            icon={<AirportShuttle className={classes.icon} />}
            title={"KOMBI PREVOZ"}
            text={"Vršimo usluge prevoza putnika sa kombi vozilom (8+1) iz Novog Sada na sve destinacije u državi i inostranstvu, brzo, kvalitetno i sigurno."}
          />
        </Grid>
        <Grid item xs={3}>
          <MediaCard
            icon={<FlightTakeoff className={classes.icon} />}
            title={"PREVOZ DO AERODROMA"}
            text={"Po dogovoru vršimo vanredne vožnje i prevoz do lokacija poput aerodroma."}
          />
        </Grid>
      </Grid>
    </div>
  );
}