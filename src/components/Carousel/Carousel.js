import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';
import { makeStyles } from '@material-ui/core/styles';
import { Button } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    img : {
      maxHeight: 500,
      width: '100%'
    },
    legend : {
      fontSize: 40,
      textTransform: 'uppercase',
      top: '30%',
      position: 'absolute',
      width: '60%',
      left: '20%',
      padding: 30,
      // top: '200px !important',
      color : "#ffc61a",
      backgroundColor: 'rgba(0,0,0,0.43) !important',
      borderRadius: '80px',
      // border: "1px solid red"
    },
    button : {
      position: 'absolute',
      top: '70%',
      width: '20%',
      left: '40%',
      backgroundColor: '#ffc61a',
      color: 'black',
      // fontWeight: 'bold',
      fontSize: '18px',
      padding: '15px 0px',
      '&:hover' : {
        backgroundColor: '#e5f12d'
      }
    }

}));

export default function CarouselSlider(props)
{
    const classes = useStyles();

    let items = [
      {
        img: "/img/pozadina2.png",
        legend: "Redovne linijske vožnje svakog dana"
      },
      {
        img: "/img/pozadina3.png",
        legend: "Brzo i bezbedno do cilja"
      },
      {
        img: "/img/pozadina4.png",
        legend: "Bezbedno u svim vremenskim uslovima"
      }
    ]

    return (
            <Carousel showThumbs={false} infiniteLoop={true} autoPlay={true} transitionTime={1000}> 
                {items.map( item=> {
                   return (<div >
                            <img className={classes.img} src={item.img} />
                            <p className={classes.legend}> {item.legend} </p>
                            <Button variant="contained" color="secondary" className={classes.button} href="/orders">Zakaži vožnju</Button>
                          </div>)
                  }) }
            </Carousel>
        );
}
