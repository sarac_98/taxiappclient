import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Paper, Button } from '@material-ui/core';
// import Paper from '@material-ui/core/Paper';
// import Grid from '@material-ui/core/Grid';
// import nbImage from '../../novibecej.png';
import Contact from '../Contact/Contact';
import { Facebook, Twitter, Instagram, LinkedIn } from '@material-ui/icons';

const useStyles = makeStyles((theme) => ({
    root: {
        height: 600,
        backgroundColor: "#000000c7",
        padding: 20,
        marginTop: 50
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: 'white',
        backgroundColor: 'inherit',
        boxShadow: 'none'
    },
    image : {
        width: 140
    },
    socialIcon : {
        fontSize: 50,
        margin: '0px 6px',
        cursor: 'pointer',
        '&:hover' : {
            color: '#ffc61a',
        }
    },
    button: {
        backgroundColor: 'inherit',
        border: '1px solid white',
        marginTop: 27,
        padding: '15px 25px',
        color: 'white',
        fontSize: 17,
        borderRadius: 5,
        cursor: 'pointer',
        fontWeight: 'bold',
        '&:hover' : {
            color: '#ffc61a',
            borderColor: '#ffc61a'
        }   
    }
}));

export default function AboutUs(props) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
             <Grid container spacing={3}>
                <Grid item xs={6} sm={3}>
                    <Paper className={classes.paper}>
                         <img className={classes.image} src='/img/novibecej.png'/>
                         <br/>
                         Neka adresa 15, Novi Bečej
                    </Paper>
                </Grid>
                <Grid item xs={6} sm={3}>
                    <Paper className={classes.paper}>
                        <button className={classes.button}>Zakaži vožnju</button>
                        <h1>062/580-500</h1>
                        <h1>069/199-80-50</h1>
                        <Facebook className={classes.socialIcon}/> 
                        <LinkedIn className={classes.socialIcon}/>
                        <Instagram className={classes.socialIcon}/> 
                        <Twitter className={classes.socialIcon}/>
                    </Paper>
                </Grid>
                <Grid item xs={6} sm={3}>
                    <Paper className={classes.paper}><Contact/></Paper>
                </Grid>
                <Grid item xs={6} sm={3}>
                    <Paper className={classes.paper}>
                        <img className={classes.image} src='/img/novisad.png'/>
                        <br/>
                        
                        Sedam Jugovića 13, Novi Sad
                    </Paper>
                </Grid>
            </Grid>
          
        </div>
    )
}