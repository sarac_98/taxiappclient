import Button from '@material-ui/core/Button';
import AddBoxIcon from '@material-ui/icons/AddBox';
import Tooltip from '@material-ui/core/Tooltip';
import AssignmentIcon from '@material-ui/icons/Assignment';
import axios from '../../utils/axios';
import fileDownload from "js-file-download";
import Progress from './Progress';
import CircularProgress from '@material-ui/core/CircularProgress';

import { Fragment, useState } from 'react';
export default function AddButton(props) {
    const [loading, setLoading] = useState(false);

    const createdPdf = async () => {

        await axios.get('/usersToServices/createPdfUserServices').then(

        );
        setLoading(true)
        setTimeout(() => {

            axios
                .get("/pdf", {
                    responseType: "blob",
                })
                .then((res) => {
                    console.log(res);
                    fileDownload(res.data, 'VožnjeZaDanas.pdf');
                    setLoading(false)
                });

        }, 1000);


    }
    const buttonStyle = {
        float: 'right',
        zIndex: 200,
        color: 'orange',
        padding: 0,
        marginTop: 15,
        marginRight: 15,
        marginRightfontSize: '30px',
        minWidth: '30px'
    }

    return (
        <Fragment>

            {!loading ? (<Tooltip title={'Red voznje za danas'}>
                <Button style={buttonStyle}>
                    <AssignmentIcon onClick={() => createdPdf()} fontSize="Large" />
                </Button>
            </Tooltip>) : <Button disabled style={buttonStyle}>
                <CircularProgress/>
            </Button>}


        </Fragment>

    )
}