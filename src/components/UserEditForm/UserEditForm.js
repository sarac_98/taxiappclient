import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/grid';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  actions: {
    marginTop: '20px'
  },
  button: {
    backgroundColor: 'rgb(199,0,0)'
  },
  textArea: {
    height:'200px',
    width:'700px'
  }

  
})


export default function UserEditForm({ user, closeEdit, update }) {

  const classes = useStyles();
  const [userName, setUserName] = useState(user.full_name);
  const [userPhone, setUserPhone] = useState(user.phone_number);
  const [userAdress, setUserAdress] = useState(user.adress);
  const [description, setDescription] = useState(user.description)

  const handleUserNameChange = (event) => {
    setUserName(event.target.value)
  };

  const handleUserPhoneChange = (event) => {
    setUserPhone(event.target.value)
  };

  const handleUserAdressChange = (event) => {
    setUserAdress(event.target.value)
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    update({
      full_name: userName,
      phone_number: userPhone,
      adress: userAdress,
      description: description
    },
      user);
    closeEdit();
  }
  return (
    <form className={classes.form} onSubmit={handleSubmit} noValidate>
      <h3>Izmena profila</h3>
      <TextField
        margin="normal"
        required
        fullWidth
        id="full_name"
        label="Ime i prezime"
        name="full_name"
        value={userName}
        onChange={handleUserNameChange}
      />
      <TextField
        margin="normal"
        required
        fullWidth
        name="phone_number"
        // inputRef={register}
        label="Broj telefona"
        id="phone_number"
        value={userPhone}
        onChange={handleUserPhoneChange}
      />
      <TextField
        margin="normal"
        required
        fullWidth
        name="adress"
        // inputRef={register}
        label="Adresa"
        id="adress"
        value={userAdress}
        onChange={handleUserAdressChange}
      />
      <textarea className={classes.textArea}
        type="text" name="message" id="message" value={description} onChange={e => setDescription(e.target.value)} />

      <Grid container spacing={1} className={classes.actions}>
        <Grid item xs={6}>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
          // className={classes.submit}
          >
            Potvrdi
          </Button>
        </Grid>
        <Grid item xs={6}>
          <Button
            type="reset"
            fullWidth
            variant="contained"
            color="secondary"
            className={classes.button}
            onClick={closeEdit}
          >
            Odustani
          </Button>
        </Grid>
      </Grid>

    </form>
  )
}