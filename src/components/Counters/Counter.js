import React, {useEffect, useState, useRef} from 'react';
import {makeStyles} from '@material-ui/core';


const useStyles = makeStyles(()=>({

    count: {
        fontSize : "6em",
        color: '#c79600',
        margin: '10px 0px 1px',
    }, title : {
        fontSize: '1.3em',
        color: 'black',
    }
}))

export default function Counter(props) {
    const {label, number, duration} = props.data;
    const [count, setCount] = useState("0")
    const [started, setStarted] = useState(false);
    const element = useRef(null);
    const classes = useStyles();


    const scrollPosition = () => {
            const e = element.current; 
            if ((window.pageYOffset + window.innerHeight + 40) > e.offsetTop) {
                    setStarted(true)
            }
    }
        
    useEffect(()=>{
            scrollPosition();
            function watchScroll() {
                    window.addEventListener("scroll", scrollPosition);
            }
            watchScroll();
            return () => {
                window.removeEventListener("scroll", scrollPosition);
            }
    }, [])
    
    useEffect(()=>{
        if (started) {
                let start = 0;
                const end = parseInt(number.substring(0,3))

                if(start == end) return;

                let totalMilSecDur = parseInt(duration);
                let incrementTime = (totalMilSecDur / end) * 1000;

                let timer = setInterval(() => {
                    start += 1;
                    setCount(String(start) + number.substring(3))
                    if (start == end) clearInterval(timer);
                }, incrementTime)
        }
            return () => {
                setCount("0")
            }
    }, [started]);

    return (
        <>
        <div className={classes.root} ref={element}>
            <h1 className={classes.count}>
                <i>{count}</i>
            </h1>
            <h3 className={classes.title}>{label}</h3>
        </div>
        </>
    )
}