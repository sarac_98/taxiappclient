import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Counter from './Counter';
import { PhoneEnabled } from '@material-ui/icons';
import PetsIcon from '@material-ui/icons/Pets';
import LocalTaxiIcon from '@material-ui/icons/LocalTaxi';
import FlightTakeoff from '@material-ui/icons/FlightTakeoff';
import AirportShuttle from '@material-ui/icons/AirportShuttle';
import MediaCard from '../../containers/Home/MediaCard';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(2),
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  paperCounter: {
    padding: theme.spacing(2),
    marginTop: 40,
    textAlign: 'center',
    color: theme.palette.text.secondary,
    // backgroundColor: '#7f0000'
    backgroundColor: 'inherit',
    boxShadow: 'none'
  },
  paperAbout: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    boxShadow: 'none'
  },
  number: {
    fontSize: '5em'
  },
  contactTel: {
    padding: '40px 0px',
    fontSize: '1.7em',
    position: 'relative',
    boxShadow: 'none'
  },
  phoneIcon: {
    position: 'absolute',
    fontSize: 200,
    bottom: 30,
    right: 40,
    color: 'gray'
  }
}));

export default function Counters() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={12} md={4}>
          <Paper className={classes.paperCounter}>
            <Counter
              data={{
                label: "Godina iskustva",
                number: "12",
                duration: "1"
              }}
            />
          </Paper>
        </Grid>
        <Grid item xs={12} sm={12} md={4}>
          <Paper className={classes.paperCounter}>
            <Counter
              data={{
                label: "Voznji nedeljno",
                number: "18",
                duration: "2"
              }}
            />
          </Paper>
        </Grid>
        <Grid item xs={12} sm={12} md={4}>
          <Paper className={classes.paperCounter}>
            <Counter
              data={{
                label: "Zadovoljnih musterija",
                number: "3240",
                duration: "4"
              }}
            />
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}