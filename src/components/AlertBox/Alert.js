import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import MuiAlert from '@material-ui/lab/Alert';
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";
import { useSelector } from "react-redux";
import Snackbar from '@material-ui/core/Snackbar';
function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}
const useStyles = makeStyles(theme => ({
    root: {
        width: "500px",
        position: "fixed",
        textAlign: "center",
        margin: "0 auto",
        bottom: "10%",
        left: "calc(50% - 250px)",
        "& > * + *": {}
    }
}));

export default function IconAlerts() {
    const classes = useStyles();
    const alertMessage = useSelector(state => state.alertMessage);
    const msgType = useSelector(state => state.msgType);

    return (
        <div className={classes.root}>
            {alertMessage === "" ? null : (
                // <Alert
                //     iconMapping={{
                //         success: <CheckCircleOutlineIcon fontSize="inherit" />
                //     }}
                // >
                //     {alertMessage}
                // </Alert>
                <Snackbar open={true} autoHideDuration={6000} >
                    <Alert severity={msgType}>
                        {alertMessage}
                    </Alert>
                </Snackbar>
            )
            }
        </div >
    );
}
