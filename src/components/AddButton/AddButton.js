import Button from '@material-ui/core/Button';
import AddBoxIcon from '@material-ui/icons/AddBox';
import Tooltip from '@material-ui/core/Tooltip';


export default function AddButton(props) {
    const buttonStyle = {
        float:'right', 
        zIndex:200, 
        color:'green', 
        padding: 0, 
        marginTop: 15, 
        marginRight: 15, 
        marginRightfontSize: '30px', 
        minWidth: '30px'
    }

    return (
        <Tooltip title={props.title}>
            <Button style={buttonStyle}>
                <AddBoxIcon onClick={props.handleOpen} fontSize="Large"/>
            </Button>
        </Tooltip>
    )
}