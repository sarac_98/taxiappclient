import axios from 'axios';
const instance = axios.create({
  // baseURL: 'http://164.68.102.45:4000/',
  // baseURL: 'http://192.168.1.111:4000/',
  baseURL: 'http://localhost:4000/',
});
// 164.68.102.45

instance.defaults.headers.common[
  'Authorization'
] = `Bearer ${localStorage.getItem('jwtToken')}`;
export default instance;