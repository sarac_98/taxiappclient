import moment from 'moment';

export const formatDate = (val) => {
  return moment(val).format("DD.MM.YYYY ");
};

export const formatDateTime = (val) => {
  return moment(val).format("DD.MM.YYYY HH:mm ");
};

export const datebaseFormat = (val) => {
  //return moment(val).format("DD-MM-YYYY HH:mm ");
  return moment(val.format('YYYY/MM/DD HH:mm:ss')).format("YYYY-MM-DD HH:mm:ss");
};

export const emailValidation = (email) => {
  var reg = /^([a-z\d\.-]+)@([a-z\d-]+)\.([a-z]{2,8})(\.[a-z]{2,8})?$/;
  reg.test(email);
  return reg.test(email);
};


export const sortArrayOfObjects = (key, order = "asc") => {
  return function innerSort(a, b) {
    if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
      // property doesn't exist on either object
      return 0;
    }

    const varA = typeof a[key] === "string" ? a[key].toUpperCase() : a[key];
    const varB = typeof b[key] === "string" ? b[key].toUpperCase() : b[key];

    let comparison = 0;
    if (varA > varB) {
      comparison = 1;
    } else if (varA < varB) {
      comparison = -1;
    }
    return order === "desc" ? comparison * -1 : comparison;
  };
}

export const removeDuplicatesFromObjectArray = (array, propName) => {
  return array.filter(
    (thing, index, self) =>
      index === self.findIndex((t) => t?.id === thing?.id)
  );
};