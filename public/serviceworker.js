// const CACHE_NAME = "verison-1";
// const urlsToCatch = ['index.html','offline.html'];

// const self = this;
// // Install SW
// self.addEventListener('install', (event)=>{
//  event.waitUntil(
//      caches.open(CACHE_NAME)
//      .then((cache)=>{
//          console.log('Opened cache')
//         return cache.addAll(urlsToCatch);
//      })
//  )
// });

// // Listen for requests
// self.addEventListener('fetch', (event)=>{
//     event.respondWith(
//         caches.match(event.request)
//         .then(() => {
//             return fetch(event.request)
//             .catch((err) => caches.match('offline.html'))
//         })
//     )
// });

// //Activate the SW
// self.addEventListener('activate', (event)=>{
//     const catcheWhiteList = [];
//     catcheWhiteList.push(CACHE_NAME);

//     event.waitUntil(
//         caches.keys().then((cacheNames)=> Promise.all(
//             cacheNames.map((cacheName) => {
//                 if(!catcheWhiteList.includes(cacheName)){
//                     return caches.delete(cacheName);
//                 }
//             })

//         ))
//     )
// });